package de.projectsmallscope.psscore.util;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.actionbar.ActionBarManager;
import de.projectsmallscope.psscore.util.common.BukkitTime;
import de.projectsmallscope.psscore.util.common.NameSpaceFactory;
import de.projectsmallscope.psscore.util.common.UtilBlock;
import de.projectsmallscope.psscore.util.common.UtilItem;
import de.projectsmallscope.psscore.util.common.UtilMobs;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import de.projectsmallscope.psscore.util.items.display.ItemDisplayCompiler;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import de.projectsmallscope.psscore.util.json.ItemStackSerializer;
import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.Getter;
import net.crytec.inventoryapi.InventoryAPI;
import net.crytec.libs.protocol.ProtocolAPI;
import net.crytec.libs.protocol.holograms.impl.HologramManager;
import net.crytec.libs.protocol.holograms.impl.infobar.InfoBar;
import net.crytec.libs.protocol.holograms.infobars.InfoBarManager;
import net.crytec.libs.protocol.npc.NpcAPI;
import net.crytec.libs.protocol.skinclient.PlayerSkinManager;
import net.crytec.libs.protocol.tablist.TabListManager;
import net.crytec.libs.protocol.tablist.implementation.EmptyTablist;
import org.bukkit.inventory.ItemStack;

public class UtilModule implements BaseModule {

  @Getter
  private HologramManager hologramManager;
  @Getter
  private ActionBarManager actionBarManager;
  @Getter
  private InfoBarManager infoBarManager;
  @Getter
  private ProtocolAPI protocolAPI;
  @Getter
  private NpcAPI npcAPI;
  @Getter
  private TabListManager tabListManager;
  @Getter
  private PlayerSkinManager playerSkinManager;
  @Getter
  private ItemDisplayCompiler displayCompiler;

  @Override
  public void enable(final PSSCore plugin) {
    plugin.getPaperCommandManager().getCommandCompletions()
        .registerStaticCompletion("Models", Arrays.stream(Model.values()).map(Enum::toString).collect(Collectors.toList()));

    GsonProvider.register(ItemStack.class, new ItemStackSerializer());

    BukkitTime.start(plugin);
    NameSpaceFactory.init(plugin);
    UtilPlayer.init(plugin);
    UtilBlock.init(plugin);
    UtilMobs.init(plugin);
    UtilItem.init(plugin);

    InventoryAPI.init(plugin);

    this.displayCompiler = new ItemDisplayCompiler(plugin);
    plugin.getProtocolManager().addPacketListener(this.displayCompiler);
    this.hologramManager = new HologramManager(plugin);
    this.playerSkinManager = new PlayerSkinManager();
    this.actionBarManager = new ActionBarManager(plugin);
    this.infoBarManager = new InfoBarManager(plugin, (entity) -> new InfoBar(entity, this.infoBarManager));
    this.protocolAPI = new ProtocolAPI(plugin);
    this.npcAPI = new NpcAPI(plugin);
    final EmptyTablist et = new EmptyTablist(this.tabListManager);
    this.tabListManager = new TabListManager(plugin, (p) -> et);
  }

  @Override
  public void disable(final PSSCore plugin) {

  }

}
