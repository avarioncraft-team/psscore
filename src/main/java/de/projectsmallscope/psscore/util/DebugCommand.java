package de.projectsmallscope.psscore.util;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 25.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
// TODO remove
@CommandAlias("debug")
@CommandPermission("admin.debug")
public class DebugCommand extends BaseCommand {

  @Default
  public void onDefault(final CommandSender sender) {

  }

  @Subcommand("models get")
  @CommandCompletion("@Models")
  public void onModelItem(final Player sender, @Values("@Models") final Model skin) {
    sender.getInventory().addItem(skin.getItem());
    Msg.send(sender, "Debug", "Du erhälst das Item zu: " + skin);
  }

  @Subcommand("head now")
  @CommandCompletion("@CachedPlayer")
  public CompletableFuture<?> onHeadNow(final Player sender, @Values("@CachedPlayer") final String cachedPlayerName) {
    return PSSCore.getModule(PlayerDataModule.class)
        .getDataByName(cachedPlayerName)
        .thenApply(Optional::orElseThrow)
        .thenAccept(pssPlayer -> sender.getInventory().addItem(pssPlayer.getLastSeenHead()));
  }

  @Subcommand("head all")
  @CommandCompletion("@CachedPlayerAll")
  public CompletableFuture<?> onHead(final Player sender, @Values("@CachedPlayerAll") final String cachedPlayerName) {
    return PSSCore.getModule(PlayerDataModule.class)
        .getDataByName(cachedPlayerName)
        .thenApply(Optional::orElseThrow)
        .thenAccept(pssPlayer -> sender.getInventory().addItem(pssPlayer.getLastSeenHead()));
  }

}
