package de.projectsmallscope.psscore.util.guis;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import de.projectsmallscope.psscore.util.tasks.TaskManager;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.anvil.AnvilGUI;
import net.crytec.inventoryapi.anvil.Response;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 28.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class PSSPlayerSelector implements InventoryProvider {

  public static void open(final Player player, final Consumer<PSSPlayer> consumer) {
    PSSPlayerSelector.open(player, consumer, (p) -> true);
  }

  public static void open(final Player player, final Consumer<PSSPlayer> consumer, final Predicate<PSSPlayer> filter) {
    final PlayerDataModule playerDataModule = PSSCore.getModule(PlayerDataModule.class);
    final List<PSSPlayer> players = Bukkit.getOnlinePlayers().stream()
        .filter(p -> p != player)
        .map(playerDataModule::getOnlineData)
        .collect(Collectors.toList());
    SmartInventory.builder()
        .size(5)
        .title("Spieler auswählen")
        .provider(new PSSPlayerSelector(players, consumer))
        .build()
        .open(player);
  }

  private final List<PSSPlayer> players;
  private final Consumer<PSSPlayer> consumer;

  @Override
  public void init(final Player player, final InventoryContent content) {
    this.players.stream().map(this::getPlayerIcon).forEach(content::add);
    content.set(SlotPos.of(4, 8), this.getInputIcon());
    content.set(SlotPos.of(4, 0), this.getBackIcon());
  }

  private ClickableItem getPlayerIcon(final PSSPlayer pssPlayer) {
    final ItemStack item = new ItemBuilder(pssPlayer.getLastSeenHead()).name("§f" + pssPlayer.getLastSeenName()).build();
    return ClickableItem.of(item, event -> {
      this.consumer.accept(pssPlayer);
      UtilPlayer.playUIClick((Player) event.getWhoClicked());
    });
  }

  private ClickableItem getInputIcon() {
    final ItemStack icon = new ItemBuilder(Material.WRITABLE_BOOK).name("§eNamen eingeben").build();
    return ClickableItem.of(icon, this::onNameInput);
  }

  private ClickableItem getBackIcon() {
    final ItemStack icon = new ItemBuilder(Model.BLACK_ARROW_LEFT.getItem()).name("§eZurück").build();
    return ClickableItem.of(icon, event -> {
      this.consumer.accept(null);
      UtilPlayer.playUIClick((Player) event.getWhoClicked());
    });
  }

  private void onNameInput(final InventoryClickEvent event) {
    UtilPlayer.playUIClick((Player) event.getWhoClicked());
    new AnvilGUI.Builder().item(new ItemBuilder(Material.WRITABLE_BOOK).name("§eSpieler namen eingeben").build())
        .title("Spieler namen eingeben")
        .onClose(pl -> PSSPlayerSelector.open(pl, this.consumer))
        .onComplete((p, in) -> {
          final CompletableFuture<Optional<PSSPlayer>> targetFuture = PSSCore.getModule(PlayerDataModule.class).getDataByName(in);
          targetFuture.thenAccept(TaskManager.getInstance().consumeBukkitSync(
              target -> target.ifPresentOrElse(this.consumer, () -> Msg.error(p, "Daten", "Dieser Spieler ist unbekannt."))));
          return Response.close();
        }).open((Player) event.getWhoClicked());
  }

}