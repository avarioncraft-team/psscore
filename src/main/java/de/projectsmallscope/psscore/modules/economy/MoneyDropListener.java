package de.projectsmallscope.psscore.modules.economy;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemMergeEvent;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class MoneyDropListener implements Listener {

  private final EconomyModule economyModule;

  @EventHandler
  public void onPickup(final EntityPickupItemEvent event) {
    final Item item = event.getItem();
    final Optional<Double> optionalValue = this.economyModule.getMoneyValue(item);
    if (!optionalValue.isPresent()) {
      return;
    }
    if (!(event.getEntity() instanceof Player)) {
      event.setCancelled(true);
      return;
    }
    final Set<String> tags = item.getScoreboardTags();
    if (tags.contains("PICKED_UP") || tags.contains("IS_COMBINED")) {
      event.setCancelled(true);
      return;
    }
    final Player player = (Player) event.getEntity();
    final PSSPlayer pssPlayer = PSSPlayer.of(player);
    item.getScoreboardTags().add("PICKED_UP");
    item.remove();
    event.setCancelled(true);
    pssPlayer.getEconomyAccount().getDefaultValue().add(optionalValue.get());
    if (pssPlayer.getPlayerSettings().isNotifyOnMoneyPickup()) {
      final String moneyEl = PSSCore.getModule(EconomyModule.class).formatString(optionalValue.get(), true);
      pssPlayer.sendMessage("Geld", "Du hast " + moneyEl + " eingesammelt.");
      UtilPlayer.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.1F, 3.0F);
    }
  }

  @EventHandler
  public void onCombine(final ItemMergeEvent event) {
    final Set<String> entTags = event.getEntity().getScoreboardTags();
    final Set<String> tarTags = event.getTarget().getScoreboardTags();
    if (entTags.contains("MONEY_ITEM") || tarTags.contains("MONEY_ITEM")) {
      event.setCancelled(true);
    }
  }

}
