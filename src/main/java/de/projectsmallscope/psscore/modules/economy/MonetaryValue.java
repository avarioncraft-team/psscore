package de.projectsmallscope.psscore.modules.economy;

import lombok.Getter;
import lombok.Setter;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class MonetaryValue {

  @Getter
  @Setter
  private double numberValue;

  public void add(final double value) {
    this.numberValue += value;
  }

  public void add(final MonetaryValue value) {
    this.add(value.numberValue);
  }

  public void subtract(final double value) {
    this.numberValue -= value;
  }

  public void subtract(final MonetaryValue value) {
    this.subtract(value.numberValue);
  }

  public boolean isBiggerOrEqual(final double value) {
    return this.numberValue >= value;
  }

  public boolean isBiggerOrEqual(final MonetaryValue value) {
    return this.isBiggerOrEqual(value.numberValue);
  }

}