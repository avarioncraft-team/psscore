package de.projectsmallscope.psscore.modules.economy;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.io.PSSConfiguration;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.Msg;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.lang.mutable.MutableDouble;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@SuppressWarnings("UnstableApiUsage")
public class EconomyModule implements BaseModule {

  public static String MONEY_NAME_SINGULAR = "X";
  public static String MONEY_NAME_PLURAL = "X";

  public static double round(final double value) {
    return ((int) (value * 100)) / 100D;
  }

  public static String formatString(double value, final boolean asElement) {
    value = EconomyModule.round(value);
    final String moneyName = value == 1 ? EconomyModule.MONEY_NAME_SINGULAR : EconomyModule.MONEY_NAME_PLURAL;
    final String formatted = EconomyModule.withSuffix(value) + " " + moneyName;
    return asElement ? Msg.elem(formatted) : formatted;
  }

  public static String withSuffix(final double count) {
    if (count < 1000) {
      return "" + count;
    }
    final int exp = (int) (Math.log(count) / Math.log(1000));
    return String.format("%.1f%c", count / Math.pow(1000, exp), "kMGTPE".charAt(exp - 1));
  }

  public static String colorPrefix(final double amount) {
    return PSSCore.getModule(EconomyModule.class).moneyPrefixRangeMap.get(amount);
  }

  private final RangeMap<Double, Model> moneyModelRangeMap = TreeRangeMap.create();
  private final RangeMap<Double, Model> moneyModelIconRangeMap = TreeRangeMap.create();
  private final RangeMap<Double, String> moneyPrefixRangeMap = TreeRangeMap.create();
  private double combinationRadius;
  private NamespacedKey moneyKey;

  public ItemStack getMoneyIcon(final double amount) {
    return Objects.requireNonNull(this.moneyModelIconRangeMap.get(amount)).getItem();
  }

  public Item dropMoney(final Location location, final double amount) {
    final MutableDouble mutableValue = new MutableDouble(amount);
    location.getNearbyEntitiesByType(Item.class, this.combinationRadius, item -> {
      final Set<String> tags = item.getScoreboardTags();
      return tags.contains("MONEY_ITEM") && !item.getScoreboardTags().contains("IS_COMBINED");
    }).forEach(item -> {
      item.getScoreboardTags().add("IS_COMBINED");
      item.remove();
      final Optional<Double> optionalDouble = this.getMoneyValue(item);
      optionalDouble.ifPresent(mutableValue::add);
    });
    final Item item = location.getWorld()
        .dropItemNaturally(location, Objects.requireNonNull(this.moneyModelRangeMap.get(mutableValue.doubleValue())).getItem());
    item.setCustomName(
        this.moneyPrefixRangeMap.get(mutableValue.doubleValue()) + EconomyModule.withSuffix(mutableValue.doubleValue()) + " $");
    item.setCustomNameVisible(true);
    item.getScoreboardTags().add("MONEY_ITEM");
    item.getPersistentDataContainer().set(this.moneyKey, PersistentDataType.DOUBLE, mutableValue.doubleValue());
    return item;
  }

  public Optional<Double> getMoneyValue(final Item item) {
    return Optional.ofNullable(item.getPersistentDataContainer().get(this.moneyKey, PersistentDataType.DOUBLE));
  }

  @Override
  public void enable(final PSSCore plugin) {
    this.moneyKey = new NamespacedKey(plugin, "MONEY_ITEM");
    this.initRangeMap();
    final PSSConfiguration configuration = plugin.getPssIO().loadConfiguration();
    EconomyModule.MONEY_NAME_SINGULAR = configuration.getMoneyNameSingular();
    EconomyModule.MONEY_NAME_PLURAL = configuration.getMoneyNamePlural();
    this.combinationRadius = configuration.getMoneyCombinationRadius();
    PSSCore.registerListener(new MoneyDropListener(this));
    plugin.getPaperCommandManager().registerCommand(new MoneyCommand());
  }

  @Override
  public void disable(final PSSCore plugin) {

  }

  private void initRangeMap() {

    this.moneyModelRangeMap.put(Range.lessThan(1D), Model.COPPER_PILE_TINY);
    this.moneyModelRangeMap.put(Range.closedOpen(1D, 5D), Model.COPPER_PILE_SMALL);
    this.moneyModelRangeMap.put(Range.closedOpen(5D, 10D), Model.COPPER_PILE_MEDIUM);
    this.moneyModelRangeMap.put(Range.closedOpen(10D, 20D), Model.COPPER_PILE_BIG);
    this.moneyModelRangeMap.put(Range.closedOpen(20D, 50D), Model.COPPER_PILE_HUGE);

    this.moneyModelRangeMap.put(Range.closedOpen(50D, 100D), Model.SILVER_PILE_TINY);
    this.moneyModelRangeMap.put(Range.closedOpen(100D, 200D), Model.SILVER_PILE_SMALL);
    this.moneyModelRangeMap.put(Range.closedOpen(200D, 500D), Model.SILVER_PILE_MEDIUM);
    this.moneyModelRangeMap.put(Range.closedOpen(500D, 1000D), Model.SILVER_PILE_BIG);
    this.moneyModelRangeMap.put(Range.closedOpen(1000D, 2000D), Model.SILVER_PILE_HUGE);

    this.moneyModelRangeMap.put(Range.closedOpen(2000D, 5000D), Model.GOLD_PILE_TINY);
    this.moneyModelRangeMap.put(Range.closedOpen(5000D, 10000D), Model.GOLD_PILE_SMALL);
    this.moneyModelRangeMap.put(Range.closedOpen(10000D, 20000D), Model.GOLD_PILE_MEDIUM);
    this.moneyModelRangeMap.put(Range.closedOpen(20000D, 50000D), Model.GOLD_PILE_BIG);
    this.moneyModelRangeMap.put(Range.closedOpen(50000D, 250000D), Model.GOLD_PILE_HUGE);

    this.moneyModelRangeMap.put(Range.closedOpen(250000D, 500000D), Model.GOLD_PILE_BAR_SMALL);
    this.moneyModelRangeMap.put(Range.closedOpen(500000D, 1000000D), Model.GOLD_PILE_BAR_MEDIUM);
    this.moneyModelRangeMap.put(Range.atLeast(1000000D), Model.GOLD_PILE_BAR_BIG);

    this.moneyModelIconRangeMap.put(Range.lessThan(1D), Model.COINS_COPPER_0);
    this.moneyModelIconRangeMap.put(Range.closedOpen(1D, 5D), Model.COINS_COPPER_1);
    this.moneyModelIconRangeMap.put(Range.closedOpen(5D, 10D), Model.COINS_COPPER_3);
    this.moneyModelIconRangeMap.put(Range.closedOpen(10D, 20D), Model.COINS_COPPER_4);
    this.moneyModelIconRangeMap.put(Range.closedOpen(20D, 50D), Model.COINS_COPPER_5);

    this.moneyModelIconRangeMap.put(Range.closedOpen(50D, 100D), Model.COINS_SILVER_0);
    this.moneyModelIconRangeMap.put(Range.closedOpen(100D, 200D), Model.COINS_SILVER_1);
    this.moneyModelIconRangeMap.put(Range.closedOpen(200D, 500D), Model.COINS_SILVER_3);
    this.moneyModelIconRangeMap.put(Range.closedOpen(500D, 1000D), Model.COINS_SILVER_4);
    this.moneyModelIconRangeMap.put(Range.closedOpen(1000D, 2000D), Model.COINS_SILVER_5);

    this.moneyModelIconRangeMap.put(Range.closedOpen(2000D, 5000D), Model.COINS_GOLD_0);
    this.moneyModelIconRangeMap.put(Range.closedOpen(5000D, 10000D), Model.COINS_GOLD_1);
    this.moneyModelIconRangeMap.put(Range.closedOpen(10000D, 20000D), Model.COINS_GOLD_2);
    this.moneyModelIconRangeMap.put(Range.closedOpen(20000D, 50000D), Model.COINS_GOLD_3);
    this.moneyModelIconRangeMap.put(Range.closedOpen(50000D, 250000D), Model.COINS_GOLD_4);

    this.moneyModelIconRangeMap.put(Range.closedOpen(250000D, 500000D), Model.COINS_GOLD_5);
    this.moneyModelIconRangeMap.put(Range.closedOpen(500000D, 1000000D), Model.BARS_GOLD_1);
    this.moneyModelIconRangeMap.put(Range.atLeast(1000000D), Model.BARS_GOLD_2);

    this.moneyPrefixRangeMap.put(Range.lessThan(5D), "§7");
    this.moneyPrefixRangeMap.put(Range.closedOpen(5D, 500D), "§f");
    this.moneyPrefixRangeMap.put(Range.closedOpen(500D, 10000D), "§a");
    this.moneyPrefixRangeMap.put(Range.closedOpen(10000D, 50000D), "§9");
    this.moneyPrefixRangeMap.put(Range.closedOpen(50000D, 1000000D), "§b");
    this.moneyPrefixRangeMap.put(Range.closedOpen(1000000D, 10000000D), "§e");
    this.moneyPrefixRangeMap.put(Range.atLeast(10000000D), "§6");

  }

}