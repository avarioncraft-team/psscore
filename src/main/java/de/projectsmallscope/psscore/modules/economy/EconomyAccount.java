package de.projectsmallscope.psscore.modules.economy;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PlayerDataComponent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class EconomyAccount extends PlayerDataComponent {

  private static final String DEFAULT_POUCH = "Geldbeutel";

  public EconomyAccount(final UUID playerID) {
    super(playerID);
    this.monetaryValueMap = new HashMap<>();
    this.createContainer(EconomyAccount.DEFAULT_POUCH);
  }

  private final Map<String, MonetaryValue> monetaryValueMap;

  public List<String> listFormattedValues() {
    final List<String> values = new ArrayList<>();
    final EconomyModule economyModule = PSSCore.getModule(EconomyModule.class);
    for (final Entry<String, MonetaryValue> entry : this.monetaryValueMap.entrySet()) {
      final String format = "§f- §e" + entry.getKey() + ": §f" + EconomyModule.formatString(entry.getValue().getNumberValue(), false);
      values.add(format);
    }
    return values;
  }

  public String getFormattedCapital(final boolean asElement) {
    return EconomyModule.formatString(this.getFullCapital(), asElement);
  }

  public double getFullCapital() {
    return this.monetaryValueMap.values().stream().mapToDouble(MonetaryValue::getNumberValue).sum();
  }

  public MonetaryValue getValue(final String pouchName) {
    return this.monetaryValueMap.get(pouchName);
  }

  public MonetaryValue getDefaultValue() {
    return this.getValue(EconomyAccount.DEFAULT_POUCH);
  }

  public MonetaryValue createContainer(final String name) {
    final MonetaryValue value = new MonetaryValue();
    final MonetaryValue removed = this.monetaryValueMap.put(name, value);
    if (removed != null) {
      value.add(removed);
    }
    return value;
  }

  public void deleteContainer(final String name) {
    if (name.equals(EconomyAccount.DEFAULT_POUCH)) {
      throw new IllegalArgumentException("Cant remove default container.");
    }
    final MonetaryValue removed = this.monetaryValueMap.remove(name);
    if (removed != null) {
      this.getDefaultValue().add(removed);
    }
  }

}
