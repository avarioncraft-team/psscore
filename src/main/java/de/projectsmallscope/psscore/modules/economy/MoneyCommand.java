package de.projectsmallscope.psscore.modules.economy;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("money|geld")
public class MoneyCommand extends BaseCommand {

  @Default
  public void onDefault(final Player sender) {
    final PSSPlayer pssSender = PSSPlayer.of(sender);
    pssSender.sendMessage("Geld", "Dein Vermögen:");
    for (final String formatted : pssSender.getEconomyAccount().listFormattedValues()) {
      sender.sendMessage(formatted);
      sender.sendMessage("");
      sender.sendMessage("§eGesamtvermögen: §f" + pssSender.getEconomyAccount().getFormattedCapital(false));
    }
  }

  // TODO make not for admins and remove from Geldbeutel
  @Subcommand("drop")
  @CommandPermission("admin")
  public void onDrop(final Player sender, double amount) {
    amount = EconomyModule.round(amount);
    final double distance = 8;
    final Block block = UtilPlayer.getBlockLookingAt(sender, distance);
    if (block == null) {
      Msg.send(sender, "Geld", "Du musst auf einen Block schauen. (Range: " + distance + ")");
      return;
    }
    final EconomyModule economyModule = PSSCore.getModule(EconomyModule.class);
    economyModule.dropMoney(block.getLocation().add(0.5, 1.1, 0.5), amount);
    Msg.send(sender, "Geld", "Du hast " + EconomyModule.formatString(amount, true) + " gedroppt.");
  }

  @Subcommand("send")
  @CommandCompletion("@CachedPlayer")
  public CompletableFuture<?> onSend(final Player sender, @Values("@CachedPlayer") final String target, final double amount) {
    if (sender.getName().equals(target)) {
      Msg.send(sender, "Geld", "Du kannst dir nicht selbst Geld zuschicken.");
      return null;
    }
    final double rounded = EconomyModule.round(amount);
    final PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);
    final PSSPlayer pssSender = dataModule.getOnlineData(sender);
    final EconomyAccount senderAccount = pssSender.getEconomyAccount();
    final MonetaryValue senderValue = senderAccount.getDefaultValue();
    if (!senderValue.isBiggerOrEqual(amount)) {
      pssSender.sendMessage("Geld", "So viel Geld hast du nicht bei dir.");
      return null;
    }
    return dataModule.getDataByName(target).thenApply(Optional::orElseThrow).thenAccept(pssTarget -> {
      final EconomyAccount targetAccount = pssTarget.getEconomyAccount();
      final MonetaryValue targetMoney = targetAccount.getDefaultValue();
      targetMoney.add(rounded);
      senderValue.subtract(rounded);
      final String amountEl = EconomyModule.formatString(rounded, true);
      final String senderEl = Msg.elem(sender.getName());
      final String targetEl = Msg.elem(pssTarget.getLastSeenName());
      pssSender.sendMessage("Geld", "Du hast " + amountEl + " von " + senderEl + " erhalten.");
      pssSender.sendMessage("Geld", "Du hast " + amountEl + " an " + targetEl + " gesendet.");
    });
  }

}
