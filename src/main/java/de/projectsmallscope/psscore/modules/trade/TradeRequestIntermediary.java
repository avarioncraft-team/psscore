package de.projectsmallscope.psscore.modules.trade;

import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataComponent;
import de.projectsmallscope.psscore.util.Msg;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 09.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class TradeRequestIntermediary extends PlayerDataComponent {

  public TradeRequestIntermediary(final UUID ownerID) {
    super(ownerID);
    this.receivedRequests = new HashMap<>();
  }

  protected TradeRequestIntermediary() {
    super(null);
    this.receivedRequests = new HashMap<>();
  }

  private transient TradeRequestIntermediary openRequest;
  private final transient Map<String, TradeRequestIntermediary> receivedRequests;

  public List<String> getReceivedRequestNames() {
    return new ArrayList<>(this.receivedRequests.keySet());
  }

  public void sendRequest(final Player sender, final TradeRequestIntermediary intermediary) {
    final Optional<Player> optionalPlayer = this.getPlayer();
    if (optionalPlayer.isEmpty()) {
      Msg.send(sender, "Handel", "Dieser Spieler ist nicht mehr online.");
      return;
    }

    final Player target = optionalPlayer.get();

    intermediary.openRequest = this;
    this.receivedRequests.put(sender.getName(), intermediary);

    Msg.send(sender, "Handel", "Du hast eine Handelsanfrage an " + Msg.elem(target.getName()) + " gesendet.");
    Msg.send(target, "Handel", "Du hast eine Handelsanfrage von " + Msg.elem(sender.getName()) + " erhalten.");
    final TextComponent pre = new TextComponent("Klicke zum [");
    pre.setColor(Msg.MESSAGE_COLOR);
    final TextComponent acceptComponent = new TextComponent("Annehmen");
    acceptComponent.setColor(ChatColor.GREEN);
    final TextComponent divider = new TextComponent(" | ");
    divider.setColor(Msg.MESSAGE_COLOR);
    final TextComponent declineComponent = new TextComponent("Ablehnen");
    declineComponent.setColor(ChatColor.RED);
    final TextComponent end = new TextComponent("]");
    end.setColor(Msg.MESSAGE_COLOR);

    acceptComponent.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/trade accept " + sender.getName()));
    acceptComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§aKlicke zum Annehmen")));

    declineComponent.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/trade decline " + sender.getName()));
    declineComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§aKlicke zum Ablehnen")));

    Msg.sendComponents(target, "Handel", pre, acceptComponent, divider, declineComponent, end);
  }

  public void accept(final String name) {
    final TradeRequestIntermediary senderIntermediary = this.receivedRequests.get(name);
    if (senderIntermediary == null || senderIntermediary.openRequest != this) {
      this.getPlayer().ifPresent(pl -> Msg.send(pl, "Handel", "Diese Anfrage ist nicht mehr aktiv."));
      return;
    }
    this.receivedRequests.remove(name);
    senderIntermediary.openRequest = null;
    final PSSPlayer pssSender = senderIntermediary.getHost();
    final PSSPlayer pssReceiver = this.getHost();
    pssSender.sendMessage("Handel", Msg.elem(pssReceiver.getLastSeenName()) + " hat deine Handelsanfrage angenommen.");
    pssReceiver.sendMessage("Handel", "Du hast die Handelsanfrage von " + Msg.elem(pssSender.getLastSeenName()) + " angenommen.");
    this.openTradeGUI(senderIntermediary);
  }

  public void decline(final String name) {
    final TradeRequestIntermediary senderIntermediary = this.receivedRequests.get(name);
    if (senderIntermediary == null || senderIntermediary.openRequest != this) {
      this.getPlayer().ifPresent(pl -> Msg.send(pl, "Handel", "Diese Anfrage ist sowieso schon abgelaufen."));
      return;
    }
    this.receivedRequests.remove(name);
    senderIntermediary.openRequest = null;
    final PSSPlayer pssSender = senderIntermediary.getHost();
    final PSSPlayer pssReceiver = this.getHost();
    pssSender.sendMessage("Handel", Msg.elem(pssReceiver.getLastSeenName()) + " hat deine Handelsanfrage abgelehnt.");
    pssReceiver.sendMessage("Handel", "Du hast die Handelsanfrage von" + Msg.elem(pssSender.getLastSeenName()) + " abgelehnt.");
  }

  private void openTradeGUI(final TradeRequestIntermediary sender) {
    this.getPlayer().ifPresent(me -> sender.getPlayer().ifPresent(other -> TradeModule.openTradeGUI(other, me)));
  }

  protected void clear(final String senderName) {
    if (this.openRequest != null) {
      this.openRequest.receivedRequests.remove(senderName);
    }
  }

}
