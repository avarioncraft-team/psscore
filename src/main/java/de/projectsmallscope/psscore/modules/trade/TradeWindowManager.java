package de.projectsmallscope.psscore.modules.trade;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.bukkit.inventory.Inventory;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 09.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class TradeWindowManager {

  public TradeWindowManager() {
    this.openViews = new HashMap<>();
  }

  private final Map<Inventory, TradeView> openViews;

  public Optional<TradeView> getViewOf(final Inventory inventory) {
    return Optional.ofNullable(this.openViews.get(inventory));
  }

  public void addView(final Inventory inventory, final TradeView view) {
    this.openViews.put(inventory, view);
  }

  public void removeView(final Inventory inventory) {
    this.openViews.remove(inventory);
  }

}