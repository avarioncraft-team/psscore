package de.projectsmallscope.psscore.modules.trade;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Values;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.guis.PSSPlayerSelector;
import java.util.Optional;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("trade|handel")
public class TradeCommand extends BaseCommand {

  @Default
  public void onDefault(final Player sender) {
    PSSPlayerSelector.open(sender, selected -> {
      final Optional<Player> optionalPlayer = selected.getOnlinePlayer();
      if (optionalPlayer.isEmpty()) {
        Msg.send(sender, "Handel", "Dieser Spieler ist nicht mehr online.");
        return;
      }
      final Player target = optionalPlayer.get();
      PSSPlayer.of(target).getTradeRequestIntermediary()
          .sendRequest(sender, PSSPlayer.of(sender).getTradeRequestIntermediary());
    });
  }

  @Subcommand("send")
  @CommandCompletion("@players")
  public void onSend(final Player sender, @Values("@players") final OnlinePlayer target) {
    PSSPlayer.of(target.getPlayer()).getTradeRequestIntermediary().sendRequest(sender, PSSPlayer.of(sender).getTradeRequestIntermediary());
  }

  @Subcommand("accept")
  @CommandCompletion("@TradeRequests")
  public void onAccept(final Player sender, final String requesterName) {
    PSSPlayer.of(sender).getTradeRequestIntermediary().accept(requesterName);
  }

  @Subcommand("decline")
  @CommandCompletion("@TradeRequests")
  public void onDecline(final Player sender, final String requesterName) {
    PSSPlayer.of(sender).getTradeRequestIntermediary().decline(requesterName);
  }

}
