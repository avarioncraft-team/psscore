package de.projectsmallscope.psscore.modules.trade;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class TradeModule implements BaseModule {

  public static void openTradeGUI(final Player sender, final Player receiver) {
    final TradeWindowManager tradeWindowManager = PSSCore.getModule(TradeModule.class).tradeWindowManager;
    final Inventory one = Bukkit.createInventory(null, 54, "Handel mit " + receiver.getName());
    final Inventory two = Bukkit.createInventory(null, 54, "Handel mit " + sender.getName());
    final TradeView oneView = new TradeView(one, two, sender.getUniqueId(), receiver.getUniqueId());
    final TradeView twoView = new TradeView(two, one, receiver.getUniqueId(), sender.getUniqueId());
    tradeWindowManager.addView(one, oneView);
    tradeWindowManager.addView(two, twoView);
    sender.openInventory(one);
    receiver.openInventory(two);
  }

  @Getter
  private TradeWindowManager tradeWindowManager;

  @Override
  public void enable(final PSSCore plugin) {
    this.tradeWindowManager = new TradeWindowManager();
    plugin.getPaperCommandManager().getCommandCompletions().registerCompletion("@TradeRequests",
        context -> PSSPlayer.of(context.getPlayer()).getTradeRequestIntermediary().getReceivedRequestNames());
    plugin.getPaperCommandManager().registerCommand(new TradeCommand());
    PSSCore.registerListener(new TradeListener(this.tradeWindowManager));
  }

  @Override
  public void disable(final PSSCore plugin) {

  }
}
