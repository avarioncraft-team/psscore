package de.projectsmallscope.psscore.modules.trade;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.economy.EconomyModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilInv;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import de.projectsmallscope.psscore.util.tasks.TaskManager;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.util.UUID;
import net.crytec.inventoryapi.anvil.AnvilGUI;
import net.crytec.inventoryapi.anvil.Response;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 10.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class TradeView {

  private static final int VIEW_MAX_SIZE = 20;
  private static final int INV_MAX_SIZE = 64;
  private static final IntArrayList CONTENT_TO_INV_MAPPING = new IntArrayList();
  private static final IntArrayList INV_TO_CONTENT_MAPPING = new IntArrayList();

  static {
    for (int i = 0; i < TradeView.VIEW_MAX_SIZE; i++) {
      final int row = i / 4;
      final int x = i % 4;
      TradeView.CONTENT_TO_INV_MAPPING.add(row * 9 + x);
    }
    for (int i = 0; i < TradeView.INV_MAX_SIZE; i++) {
      final int row = i / 9;
      final int x = i % 9;
      TradeView.INV_TO_CONTENT_MAPPING.add(x > 3 ? -1 : (row * 4 + x));
    }
  }

  public TradeView(final Inventory view, final Inventory otherView, final UUID holderID, final UUID otherID) {
    this.content = new ItemStack[TradeView.VIEW_MAX_SIZE];
    if (view.getSize() != otherView.getSize()) {
      throw new IllegalArgumentException("Inventories must be of same size.");
    }
    this.view = view;
    this.otherView = otherView;
    this.updateMoneyIcon();
    view.setItem(45, this.getCancelIcon());
    view.setItem(4, new ItemBuilder(Model.TRADE_GUI.getItem()).name("").build());
    this.holderID = holderID;
    this.otherID = otherID;
  }

  private final UUID holderID;
  private final UUID otherID;
  private final ItemStack[] content;
  private double currentMoney;
  private final Inventory view;
  private final Inventory otherView;
  private boolean bypassed = false;

  public void cancel() {
    if (this.bypassed) {
      return;
    }
    final TradeWindowManager tradeWindowManager = PSSCore.getModule(TradeModule.class).getTradeWindowManager();
    final Entity holder = Bukkit.getEntity(this.holderID);
    final Entity other = Bukkit.getEntity(this.otherID);
    tradeWindowManager.removeView(this.view);
    tradeWindowManager.removeView(this.otherView);
    if (holder instanceof Player) {
      ((Player) holder).closeInventory();
      Msg.send(((Player) holder), "Handel", "Der Handel wurde abgebrochen.");
    }
    if (other instanceof Player) {
      ((Player) other).closeInventory();
      Msg.send(((Player) other), "Handel", "Der Handel wurde abgebrochen.");
    }
  }

  private ItemStack getCancelIcon() {
    return new ItemBuilder(Model.RED_X.getItem()).name("§cHandel abbrechen.").build();
  }

  private void toggleBypass() {
    this.bypassed = !this.bypassed;
  }

  public void handleClick(final InventoryClickEvent event) {
    event.setCancelled(true);
    final Inventory clicked = event.getClickedInventory();
    final Player who = (Player) event.getWhoClicked();
    if (clicked == null) {
      return;
    }
    UtilPlayer.playUIClick(who);
    final int slot = event.getSlot();
    if (clicked != this.view) {
      final ItemStack clickedItem = clicked.getItem(slot);
      if (clickedItem != null) {
        final int left = this.add(clickedItem);
        clickedItem.setAmount(left);
      }
    } else {
      if (slot == 45) {
        this.cancel();
        return;
      } else if (slot == 48) {
        this.toggleBypass();
        new AnvilGUI.Builder()
            .item(this.getCurrentMoneyIcon())
            .title("Geld eintragen")
            .text("" + this.currentMoney)
            .onComplete((p, in) -> {
              double val;
              try {
                val = Double.parseDouble(in);
              } catch (final NumberFormatException e) {
                val = 0;
              }
              this.setMoney(val);
              TaskManager.getInstance().runBukkitSync(() -> p.openInventory(this.view));
              return Response.close();
            }).onClose(p -> TaskManager.getInstance().runBukkitSync(() -> p.openInventory(this.view)))
            .open(who);
        return;
      }
      final ItemStack ret = this.removeItem(slot);
      if (ret != null) {
        who.getInventory().addItem(ret).values().forEach(overhead -> who.getWorld().dropItemNaturally(who.getLocation(), overhead));
      }
    }
  }

  private ItemStack getCurrentMoneyIcon() {
    final EconomyModule economyModule = PSSCore.getModule(EconomyModule.class);
    return new ItemBuilder(economyModule.getMoneyIcon(this.currentMoney))
        .name(EconomyModule.colorPrefix(this.currentMoney) + EconomyModule.formatString(this.currentMoney, false))
        .build();
  }

  private void updateMoneyIcon() {
    final ItemStack otherIcon = this.getCurrentMoneyIcon();
    final ItemStack moneyIcon = new ItemBuilder(otherIcon.clone())
        .lore("")
        .lore("§7Klicke, um einen anderen")
        .lore("§7Wert einzutragen.")
        .build();
    this.view.setItem(48, moneyIcon);
    this.otherView.setItem(50, otherIcon);
  }

  public void setMoney(final double value) {
    this.currentMoney = value;
    this.updateMoneyIcon();
  }

  protected int add(final ItemStack itemStack) {
    int left = itemStack.getAmount();
    final int max = itemStack.getMaxStackSize();
    for (int index = 0; index < this.content.length; index++) {
      final ItemStack current = this.content[index];
      if (current == null) {
        this.content[index] = itemStack;
        left = 0;
      } else if (current.getAmount() != max && current.isSimilar(itemStack)) {
        final int canAdd = max - current.getAmount();
        if (canAdd >= left) {
          left = 0;
          current.add(left);
        } else {
          current.setAmount(max);
          left -= canAdd;
        }
      }
      if (left == 0) {
        break;
      }
    }
    this.updateViews();
    return left;
  }

  protected ItemStack removeItem(final int invIndex) {
    final int index = TradeView.INV_TO_CONTENT_MAPPING.getInt(invIndex);
    if (index == -1) {
      return null;
    }
    final ItemStack item = this.content[index];
    this.view.setItem(invIndex, null);
    this.otherView.setItem(UtilInv.getVerticallyFlippedIndex(invIndex), null);
    return item;
  }

  private void updateViews() {
    for (int i = 0; i < this.view.getSize(); i++) {
      final ItemStack item = this.content[i];
      final int index = TradeView.CONTENT_TO_INV_MAPPING.getInt(i);
      this.view.setItem(index, item);
      this.otherView.setItem(UtilInv.getVerticallyFlippedIndex(index), item);
    }
  }

}