package de.projectsmallscope.psscore.modules.trade;

import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 09.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class TradeListener implements Listener {

  private final TradeWindowManager tradeWindowManager;

  @EventHandler
  public void onQuit(final PlayerQuitEvent event) {
    PSSPlayer.of(event.getPlayer()).getTradeRequestIntermediary().clear(event.getPlayer().getName());
  }

  @EventHandler
  public void onClick(final InventoryClickEvent event) {
    this.tradeWindowManager.getViewOf(event.getInventory()).ifPresent(view -> view.handleClick(event));
  }

  @EventHandler
  public void onDrag(final InventoryDragEvent event) {
    this.tradeWindowManager.getViewOf(event.getInventory()).ifPresent(ignored -> event.setCancelled(true));
  }

  @EventHandler
  public void onClose(final InventoryCloseEvent event) {
    this.tradeWindowManager.getViewOf(event.getInventory()).ifPresent(TradeView::cancel);
  }

}
