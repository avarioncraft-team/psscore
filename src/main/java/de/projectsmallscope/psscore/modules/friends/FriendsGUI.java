package de.projectsmallscope.psscore.modules.friends;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.friends.FriendAccount.FriendRequest;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import de.projectsmallscope.psscore.util.functional.FutureOperations;
import de.projectsmallscope.psscore.util.guis.ConfirmationGUI;
import de.projectsmallscope.psscore.util.guis.PSSPlayerSelector;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import de.projectsmallscope.psscore.util.tasks.TaskManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 25.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class FriendsGUI implements InventoryProvider {

  public static void open(final Player player) {
    final TaskManager taskManager = TaskManager.getInstance();
    final PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);
    final PSSPlayer pssPlayer = dataModule.getOnlineData(player);

    final List<CompletableFuture<PSSPlayer>> friendFutures = pssPlayer.getFriendAccount()
        .getFriendIDs()
        .stream()
        .map(dataModule::getData)
        .collect(Collectors.toList());

    FutureOperations.waitForAsync(friendFutures)
        .thenAccept(taskManager.consumeBukkitSync((friends) -> {
          final List<String> senderNames = pssPlayer.getFriendAccount()
              .getReceivedRequests()
              .values()
              .stream()
              .map(r -> r.senderName)
              .collect(Collectors.toList());
          try {
            FriendsGUI.buildAndOpen(player, friends, senderNames);
          } catch (final Exception e) {
            e.printStackTrace();
          }
        }));
  }

  private static void buildAndOpen(final Player player, final List<PSSPlayer> friends, final List<String> senders) {
    SmartInventory.builder()
        .size(5)
        .title("Freunde")
        .provider(new FriendsGUI(player, friends, senders))
        .build()
        .open(player);
  }

  private FriendsGUI(final Player opener, final List<PSSPlayer> friends, final List<String> senders) {
    this.opener = opener;
    this.friends = friends;
    this.pssOpener = PSSCore.getModule(PlayerDataModule.class).getOnlineData(opener);
    this.senders = senders;
  }

  private final PSSPlayer pssOpener;
  private final Player opener;
  private final List<PSSPlayer> friends;
  private final List<String> senders;

  @Override
  public void init(final Player player, final InventoryContent content) {
    this.friends.stream().map(this::friendIcon).forEach(content::add);
    content.set(SlotPos.of(4, 8), ClickableItem.of(this.buildAddItem(), this::onAddClick));
    content.set(SlotPos.of(4, 0), ClickableItem.of(this.buildRequestsItem(), this::onRequestsClick));
  }

  private ItemStack buildAddItem() {
    final ItemBuilder builder = new ItemBuilder(Model.GREEN_PLUS.getItem());

    builder.name("§aSpieler einladen");
    builder.lore("", "§7Klicke hier, um einen Spieler");
    builder.lore("§7in deine Freundesliste einzuladen.");

    return builder.build();
  }

  private void onAddClick(final InventoryClickEvent event) {
    PSSPlayerSelector.open(this.opener, selected -> {
      if (selected != null) {
        this.pssOpener.getFriendAccount().sendFriendRequestTo(selected);
      }
      TaskManager.getInstance().runBukkitSync(this.pssOpener::openFriendsGUI);
    });
  }

  private ItemStack buildRequestsItem() {
    final ItemBuilder builder;
    if (this.senders.isEmpty()) {
      builder = new ItemBuilder(Model.LETTER_NO.getItem()).name("§aEinladungen");
      builder.lore("", "§7Du hast §ekeine §7Einladungen.");
    } else {
      builder = new ItemBuilder(Model.LETTER_YES.getItem()).name("§aEinladungen");
      builder.lore("", "§7Du hast §e" + this.senders.size() + " §7Einladungen.");

      int count = 5;
      for (final String sender : this.senders) {
        if (count-- == 0) {
          break;
        }
        builder.lore("§7- " + sender);
      }
      if (count == 0) {
        builder.lore("§7...");
      }

      builder.lore("§7Klicke, um sie dir anzuschauen.");
    }
    return builder.build();
  }

  private void onRequestsClick(final InventoryClickEvent event) {
    RequestGUI.open(this);
  }

  private ClickableItem friendIcon(final PSSPlayer friend) {
    final ItemBuilder builder = new ItemBuilder(friend.getLastSeenHead());

    builder.name("§a" + friend.getLastSeenName());
    builder.lore("", "§7Linksklicke für Aktionen");
    builder.lore("", "§7Rechtsklicke, um ihn zu entfernen");

    return ClickableItem.of(builder.build(), e -> this.onFriendClick(e, friend));
  }

  private void onFriendClick(final InventoryClickEvent event, final PSSPlayer friend) {
    if (event.isRightClick()) {
      ConfirmationGUI.open((Player) event.getWhoClicked(), answer -> {
        if (answer) {
          this.pssOpener.getFriendAccount().removeFriend(friend);
        }
        FriendsGUI.open(this.opener);
      });
    } else {
      this.openFriendOptions(friend);
    }
  }

  private void openFriendOptions(final PSSPlayer friend) {
    FriendOptionGUI.open(this, friend);
  }

  @RequiredArgsConstructor
  private static class RequestGUI implements InventoryProvider {

    private static void open(final FriendsGUI parent) {
      final PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);
      final Map<UUID, FriendRequest> shallowRequestMap = parent.pssOpener.getFriendAccount().getReceivedRequests();
      final List<UUID> user = new ArrayList<>(shallowRequestMap.keySet());
      final List<CompletableFuture<PSSPlayer>> futureUsers = user.stream().map(dataModule::getData).collect(Collectors.toList());
      FutureOperations.waitNonBlocking(futureUsers).thenAccept(requesters -> {
        final Map<PSSPlayer, FriendRequest> requestMap = new HashMap<>();
        for (final PSSPlayer pssReq : requesters) {
          requestMap.put(pssReq, shallowRequestMap.get(pssReq.getPlayerID()));
        }
        RequestGUI.buildAndOpen(parent, requestMap);
      });
    }

    private static void buildAndOpen(final FriendsGUI parent, final Map<PSSPlayer, FriendRequest> requestMap) {
      SmartInventory.builder()
          .title("Anfragen")
          .size(5)
          .provider(new RequestGUI(parent, requestMap))
          .build()
          .open(parent.opener);
    }

    private final FriendsGUI parent;
    private final Map<PSSPlayer, FriendRequest> requestMap;

    @Override
    public void init(final Player player, final InventoryContent content) {
      for (final Entry<PSSPlayer, FriendRequest> entry : this.requestMap.entrySet()) {
        content.add(this.getRequestIcon(entry.getKey(), entry.getValue()));
      }
      content.set(SlotPos.of(4, 0), this.getBackIcon());
    }

    private ClickableItem getRequestIcon(final PSSPlayer requester, final FriendRequest request) {
      final ItemStack icon = new ItemBuilder(requester.getLastSeenHead())
          .name("§f" + requester.getLastSeenName())
          .lore("", "§7Linksklicke zum §aannehmen")
          .lore("§7Rechtsklicke zum §cablehnen")
          .build();
      return ClickableItem.of(icon, event -> {
        if (event.isRightClick()) {
          request.decline();
        } else {
          request.accept();
        }
        this.requestMap.remove(requester);
        UtilPlayer.playUIClick((Player) event.getWhoClicked());
        this.reopen(this.parent.opener);
      });
    }

    private ClickableItem getBackIcon() {
      final ItemStack icon = new ItemBuilder(Model.BLACK_ARROW_LEFT.getItem()).name("§eZurück").build();
      return ClickableItem.of(icon, event -> {
        UtilPlayer.playUIClick((Player) event.getWhoClicked());
        FriendsGUI.open(this.parent.opener);
      });
    }
  }

  @RequiredArgsConstructor
  private static class FriendOptionGUI implements InventoryProvider {

    private static void open(final FriendsGUI parent, final PSSPlayer friend) {
      SmartInventory.builder().size(5).title("Optionen für " + friend.getLastSeenName()).build().open(parent.opener);
    }

    private final FriendsGUI parent;
    private final PSSPlayer friend;

    @Override
    public void init(final Player player, final InventoryContent content) {
      content.set(SlotPos.of(0, 8), this.getRemoveIcon());
      content.set(SlotPos.of(4, 0), this.getBackIcon());
      content.set(SlotPos.of(2, 4), this.getHeadIcon());
    }

    private ClickableItem getBackIcon() {
      final ItemStack icon = new ItemBuilder(Model.BLACK_ARROW_LEFT.getItem()).name("§eZurück").build();
      return ClickableItem.of(icon, e -> this.parent.reopen(this.parent.opener));
    }

    private ClickableItem getRemoveIcon() {
      final ItemStack icon = new ItemBuilder(Model.RED_X.getItem()).name("§cFreund entfernen").build();
      return ClickableItem.of(icon, e -> {
        this.parent.pssOpener.getFriendAccount().removeFriend(this.friend);
        this.parent.pssOpener.openFriendsGUI();
      });
    }

    private ClickableItem getHeadIcon() {
      final ItemStack head = new ItemBuilder(this.friend.getLastSeenHead()).name("§f" + this.friend.getLastSeenName()).build();
      return ClickableItem.empty(head);
    }
  }

}