package de.projectsmallscope.psscore.modules.friends;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 25.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class FriendAccountModule implements BaseModule {

  public CompletableFuture<FriendAccount> getFriendAccount(final UUID playerID) {
    return PSSCore.getModule(PlayerDataModule.class).getData(playerID).thenApply(PSSPlayer::getFriendAccount);
  }

  @Override
  public void enable(final PSSCore plugin) {
    //GsonProvider.register(FriendAccount.class, new FriendAccountTypeAdapter());
    plugin.getPaperCommandManager().registerCommand(new FriendsCommand());
  }

  @Override
  public void disable(final PSSCore plugin) {

  }
}