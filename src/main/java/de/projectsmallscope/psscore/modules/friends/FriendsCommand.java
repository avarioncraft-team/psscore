package de.projectsmallscope.psscore.modules.friends;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 28.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("freunde|friends")
public class FriendsCommand extends BaseCommand {

  private final PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);

  @Default
  public void onDefault(final Player sender) {
    final PSSPlayer pssPlayer = this.dataModule.getOnlineData(sender);
    pssPlayer.sendMessage("Freunde", "Öffne GUI für die Freundesliste.");
    pssPlayer.openFriendsGUI();
  }

}
