package de.projectsmallscope.psscore.modules.friends;

import com.google.common.collect.Sets;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataComponent;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.bukkit.Sound;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 25.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class FriendAccount extends PlayerDataComponent {

  private final Set<UUID> friends;

  private final Map<UUID, FriendRequest> sentRequests;

  private final Map<UUID, FriendRequest> receivedRequests;

  private final transient PlayerDataModule playerDataModule;

  protected FriendAccount() {
    this(null);
  }

  public FriendAccount(final UUID holderID) {
    super(holderID);
    this.friends = new HashSet<>();
    this.sentRequests = new HashMap<>();
    this.receivedRequests = new HashMap<>();
    this.playerDataModule = PSSCore.getModule(PlayerDataModule.class);
  }

  public void removeFriend(final PSSPlayer friend) {
    if (this.friends.remove(friend.getPlayerID())) {
      final String name = Msg.elem(friend.getLastSeenName());
      this.getHost().sendMessage("Freunde", name + " ist jetzt nicht mehr dein Freund.");
      friend.getFriendAccount().removeFriend(this.playerDataModule.getOnlineData(this.ownerID));
    }
  }

  public Map<UUID, FriendRequest> getReceivedRequests() {
    return new HashMap<>(this.receivedRequests);
  }

  public Map<UUID, FriendRequest> getSentRequests() {
    return new HashMap<>(this.sentRequests);
  }

  public boolean isFriend(final UUID playerID) {
    return this.friends.contains(playerID);
  }

  public boolean isFriend(final PSSPlayer pssPlayer) {
    return this.isFriend(pssPlayer.getPlayerID());
  }

  public void sendFriendRequestTo(final PSSPlayer other) {
    if (other.getPlayerID().equals(this.ownerID)) {
      final Msg.Pack pack = new Msg.Pack("Freunde", "Witzig");
      other.sendMessage(pack.getModuleName(), pack.getMessage());
      return;
    }
    final String name = Msg.elem(other.getLastSeenName());
    this.playerDataModule.bukkitSync(this.ownerID, holder -> {
      holder.sendMessage("Freunde", "Du hast eine neue Freundschaftsanfrage an " + name + " gesendet.");
      other.getFriendAccount()
          .addFriendRequest(new FriendRequest(holder.getPlayerID(), other.getPlayerID(), holder.getLastSeenName()));
    });
  }

  private void addFriendRequest(final FriendRequest request) {
    final String name = Msg.elem(request.senderName);
    this.playerDataModule.bukkitSync(this.ownerID, holder -> {
      holder.getOnlinePlayer().ifPresent(player -> UtilPlayer.playSound(player, Sound.BLOCK_NOTE_BLOCK_PLING, 0.6F, 1.2F));
      holder.sendMessage("Freunde", "Du hast eine neue Freundschaftsanfrage von " + name + " erhalten.");
      this.receivedRequests.put(request.senderID, request);
    });
  }

  /**
   * Mutations of this set are not reflected on the underlying data structure.
   *
   * @return A copy of the underlying set.
   */
  public Set<UUID> getFriendIDs() {
    return Sets.newHashSet(this.friends);
  }

  @RequiredArgsConstructor
  public static class FriendRequest {

    private final transient PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);
    protected final UUID senderID;
    protected final UUID receiverID;
    protected final String senderName;

    public void accept() {

      this.dataModule.bukkitSyncInteract(this.senderID, this.receiverID, (sender, receiver) -> {
        final FriendAccount senderAccount = sender.getFriendAccount();
        final FriendAccount receiverAccount = receiver.getFriendAccount();

        senderAccount.friends.add(this.receiverID);
        receiverAccount.friends.add(this.senderID);

        receiverAccount.receivedRequests.remove(this.senderID);
        senderAccount.sentRequests.remove(this.receiverID);

        final String senderName = Msg.elem(sender.getLastSeenName());
        final String receiverName = Msg.elem(receiver.getLastSeenName());

        sender.sendMessage("Freunde", "Deine Freundschaftanfrage wurde von " + receiverName + " angenommen.");
        receiver.sendMessage("Freunde", "Du hast die Freundschaftsanfrage von " + senderName + " angenommen.");
      });

    }

    public void decline() {

      this.dataModule.bukkitSyncInteract(this.senderID, this.receiverID, (sender, receiver) -> {
        final FriendAccount senderAccount = sender.getFriendAccount();
        final FriendAccount receiverAccount = receiver.getFriendAccount();

        receiverAccount.receivedRequests.remove(this.senderID);
        senderAccount.sentRequests.remove(this.receiverID);

        final String senderName = Msg.elem(sender.getLastSeenName());
        final String receiverName = Msg.elem(receiver.getLastSeenName());

        sender.sendMessage("Freunde", "Deine Freundschaftanfrage wurde von " + receiverName + " abgelehnt.");
        receiver.sendMessage("Freunde", "Du hast die Freundschaftsanfrage von " + senderName + " abgelehnt.");
      });

    }
  }

}