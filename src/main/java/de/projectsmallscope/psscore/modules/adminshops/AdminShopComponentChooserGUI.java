package de.projectsmallscope.psscore.modules.adminshops;

import de.projectsmallscope.psscore.modules.adminshops.components.ItemShopComponent;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 13.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class AdminShopComponentChooserGUI implements InventoryProvider {

  public static void open(final Player player, final Consumer<AdminShopComponent> componentConsumer) {
    SmartInventory.builder()
        .title("Komponente auswählen")
        .size(3)
        .provider(new AdminShopComponentChooserGUI(componentConsumer))
        .build()
        .open(player);
  }

  private final Consumer<AdminShopComponent> componentConsumer;

  @Override
  public void init(final Player player, final InventoryContent content) {
    final ItemStack item = new ItemBuilder(Material.IRON_SWORD).name("§eItem").build();
    content.add(ClickableItem.of(item, event -> this.componentConsumer.accept(new ItemShopComponent(this.getPlaceholder(), 0))));
  }

  private ItemStack getPlaceholder() {
    return new ItemBuilder(Model.RED_X.getItem()).name("§cKlicke auf ein Item in deinem Inventar").build();
  }

}
