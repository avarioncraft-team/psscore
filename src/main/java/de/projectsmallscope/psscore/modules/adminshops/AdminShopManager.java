package de.projectsmallscope.psscore.modules.adminshops;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 12.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class AdminShopManager {

  public AdminShopManager() {
    this.adminShopMap = new HashMap<>();
  }

  private final Map<String, AdminShop> adminShopMap;

  public Optional<AdminShop> getShop(final String internalName) {
    return Optional.ofNullable(this.adminShopMap.get(internalName));
  }

  public AdminShop createShop(final String internalName, final String uiName) {
    final AdminShop adminShop = new AdminShop(internalName, uiName);
    this.adminShopMap.put(internalName, adminShop);
    return adminShop;
  }

  public void deleteShop(final String internalName) {
    this.adminShopMap.remove(internalName);
  }

}