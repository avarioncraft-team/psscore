package de.projectsmallscope.psscore.modules.adminshops.components;

import com.google.common.base.Preconditions;
import de.projectsmallscope.psscore.modules.adminshops.AdminShop;
import de.projectsmallscope.psscore.modules.adminshops.AdminShopComponent;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilInv;
import java.util.Arrays;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 13.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class ItemShopComponent extends AdminShopComponent {

  public ItemShopComponent(final ItemStack itemStack, final double baseValue) {
    super(baseValue);
    this.itemStack = itemStack.asOne();
    Preconditions.checkNotNull(itemStack);
    Preconditions.checkArgument(itemStack.getType() != Material.AIR);
  }

  @Setter
  private ItemStack itemStack;

  @Override
  public String getName() {
    return this.itemStack.getItemMeta().getDisplayName();
  }

  @Override
  public ItemStack getIcon() {
    return this.itemStack;
  }

  @Override
  public void giveTo(final PSSPlayer buyer, final int amount) {
    final int maxStackSize = this.itemStack.getMaxStackSize();
    final int stacks = amount / maxStackSize;
    final int left = amount % maxStackSize;
    if (stacks > 0) {
      for (int i = 0; i < stacks; i++) {
        final ItemStack stack = this.itemStack.clone();
        stack.setAmount(maxStackSize);
        buyer.addItem(stack);
      }
    }
    if (left > 0) {
      final ItemStack leftItem = this.itemStack.clone();
      leftItem.setAmount(left);
      buyer.addItem(leftItem);
    }
    final String countEl = Msg.elem(amount + "x " + this.getName());
    buyer.sendMessage("Shop", "Du hast " + countEl + " vom Shop erhalten.");
  }

  @Override
  public boolean has(final PSSPlayer seller, final int amount) {
    return UtilInv.contains(Arrays.asList(seller.getInventoryContent()), this.itemStack, amount);
  }

  @Override
  public void removeFrom(final PSSPlayer seller, final int amount) {
    UtilInv.remove(Arrays.asList(seller.getInventoryContent()), this.itemStack, amount);
  }

  @Override
  public void openEditor(final Player player, final AdminShop from) {
    ItemShopComponentGUI.open(player, this, from);
  }
}
