package de.projectsmallscope.psscore.modules.adminshops;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import java.util.Optional;
import lombok.Getter;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 12.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class AdminShopModule implements BaseModule {

  @Getter
  private AdminShopManager adminShopManager;

  @Override
  public void enable(final PSSCore plugin) {
    final Optional<String> data = plugin.getPssIO().loadAdminShopData();
    if (data.isPresent()) {
      this.adminShopManager = GsonProvider.fromJson(data.get(), AdminShopManager.class);
    } else {
      this.adminShopManager = new AdminShopManager();
    }
  }

  @Override
  public void disable(final PSSCore plugin) {
    plugin.getPssIO().saveAdminShopData(GsonProvider.toJson(this.adminShopManager));
  }
}