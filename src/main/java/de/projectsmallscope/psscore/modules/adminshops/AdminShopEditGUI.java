package de.projectsmallscope.psscore.modules.adminshops;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import lombok.RequiredArgsConstructor;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 13.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class AdminShopEditGUI implements InventoryProvider {

  public static void open(final Player player, final AdminShop adminShop) {
    SmartInventory.builder()
        .title("Editiere: " + adminShop.getInternalName())
        .size(5)
        .provider(new AdminShopEditGUI(adminShop))
        .build()
        .open(player);
  }

  private final AdminShop adminShop;
  private final double interestRate = PSSCore.getInstance().getPSSConfig().getItemInterestRate();

  @Override
  public void init(final Player player, final InventoryContent content) {

    for (final AdminShopComponent component : this.adminShop.getAdminShopComponentList()) {
      content.add(this.getComponentIcon(component));
    }

    content.set(SlotPos.of(4, 0), this.getAddIcon());
    content.set(SlotPos.of(4, 8), this.getLockItem());

  }

  private ClickableItem getComponentIcon(final AdminShopComponent component) {
    final ItemStack item = component.getEditorIcon(this.interestRate);
    return ClickableItem.of(item, event -> this.onComponentClick(event, component));
  }

  private ClickableItem getAddIcon() {
    final ItemStack item = new ItemBuilder(Model.GREEN_PLUS.getItem())
        .name("§eKomponente hinzufügen")
        .build();
    return ClickableItem.of(item, event -> {
      AdminShopComponentChooserGUI.open((Player) event.getWhoClicked(), component -> {
        this.adminShop.getAdminShopComponentList().add(component);
        this.adminShop.openEditor((Player) event.getWhoClicked());
      });
    });
  }

  private ClickableItem getLockItem() {
    final Model model = this.adminShop.isLocked() ? Model.LOCK_CLOSED : Model.LOCK_OPEN;
    final ItemStack item = new ItemBuilder(model.getItem())
        .name("§eGesperrt: " + (this.adminShop.isLocked() ? "§cJa" : "§aNein"))
        .lore("", "§7Klicke zum Ändern")
        .build();
    return ClickableItem.of(item, event -> {
      this.adminShop.setLocked(!this.adminShop.isLocked());
      this.reopen((Player) event.getWhoClicked());
    });
  }

  private void onComponentClick(final InventoryClickEvent event, final AdminShopComponent component) {
    if (event.isRightClick()) {
      this.adminShop.getAdminShopComponentList().remove(component);
      this.reopen((Player) event.getWhoClicked());
    } else {
      component.openEditor((Player) event.getWhoClicked(), this.adminShop);
    }
  }

}
