package de.projectsmallscope.psscore.modules.adminshops;

import de.projectsmallscope.psscore.modules.economy.EconomyAccount;
import de.projectsmallscope.psscore.modules.economy.EconomyModule;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 12.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public abstract class AdminShopComponent {

  public AdminShopComponent(final double baseValue) {
    this.baseValue = baseValue;
  }

  @Setter
  @Getter
  private double baseValue;
  @Getter
  private final long timesBought = 0L;
  @Getter
  private final long timesSold = 0L;
  @Getter
  private long heat = 0L;
  @Getter
  @Setter
  private boolean buyable = true;
  @Getter
  @Setter
  private boolean sellable = true;

  public void cooldown(final int amount) {
    if (this.heat > 0) {
      this.heat -= amount;
      if (this.heat < 0) {
        this.heat = 0;
      }
    } else {
      this.heat += amount;
      if (this.heat > 0) {
        this.heat = 0;
      }
    }
  }

  public double getSellPrice(final int amount, final double interest) {
    final double current = this.getCurrentPrice(interest);
    return current * ((1 - Math.pow(interest, amount)) / (1 - interest));
  }

  public double getBuyPrice(final int amount, final double interest) {
    final double current = this.getCurrentPrice(interest);
    return -current * ((1 - Math.pow(interest, -amount)) / (1 - interest)) / 2;
  }

  public double getCurrentPrice(final double interest) {
    return this.baseValue * Math.pow(interest, this.heat);
  }

  public boolean onPlayerBuys(final PSSPlayer buyer, final int amount, final double interest) {
    final double price = this.getSellPrice(amount, interest);
    final EconomyAccount economyAccount = buyer.getEconomyAccount();
    final String costEl = EconomyModule.formatString(price, true);
    if (!economyAccount.getDefaultValue().isBiggerOrEqual(price)) {
      buyer.sendMessage("Shop", "Du benötigst mindestens " + costEl + " in deinem Geldbeutel.");
      return false;
    }
    economyAccount.getDefaultValue().subtract(price);
    this.giveTo(buyer, amount);
    this.heat += amount;
    buyer.sendMessage("Shop", "Gekauft: " + Msg.elem(amount + "x ") + Msg.elem(this.getName()));
    buyer.sendMessage("Shop", "Kostenpunkt: " + costEl);
    return true;
  }

  public boolean onPlayerSells(final PSSPlayer seller, final int amount, final double interest) {
    final double price = this.getBuyPrice(amount, interest);
    final EconomyAccount economyAccount = seller.getEconomyAccount();
    final String costEl = EconomyModule.formatString(price, true);
    if (!this.has(seller, amount)) {
      seller.sendMessage("Shop", "Du benötigst mindestens " + Msg.elem(amount + "x ") + Msg.elem(this.getName()) + " bei dir.");
      return false;
    }
    economyAccount.getDefaultValue().add(price);
    this.removeFrom(seller, amount);
    this.heat -= amount;
    seller.sendMessage("Shop", "Verkauft: " + Msg.elem(amount + "x ") + Msg.elem(this.getName()));
    seller.sendMessage("Shop", "Einkommen: " + costEl);
    return true;
  }

  public ItemStack getCustomerIcon() {
    return null; // TODO
  }

  public ItemStack getInfoIcon(final double interestRate) {
    final Model buyPre = this.isBuyable() ? Model.GREEN_CHECK : Model.RED_X;
    final Model sellPre = this.isSellable() ? Model.GREEN_CHECK : Model.RED_X;
    return new ItemBuilder(this.getIcon())
        .lore("§eBasispreis: §f" + this.getBaseValue())
        .lore("§eSpieler zahlt: §f" + this.getCurrentPrice(interestRate))
        .lore("§eSpieler bekommt: §f" + this.getCurrentPrice(interestRate) / 2)
        .lore("§eVerkauft: §f" + this.getTimesSold())
        .lore("§eGekauft: §f" + this.getTimesBought())
        .lore("§eHitze: §f" + this.getHeat())
        .lore("", "§eKann von Shop gekauft werden: §r" + (buyPre.getBoxedFontChar().toString()))
        .lore("§eKann an Shop verkauft werden: §r" + (sellPre.getBoxedFontChar().toString()))
        .build();
  }

  public ItemStack getEditorIcon(final double interestRate) {
    return new ItemBuilder(this.getInfoIcon(interestRate))
        .lore("", "§e[§fL -> §aEditieren §f|§c Löschen §f<- R§e]")
        .build();
  }

  public abstract String getName();

  public abstract ItemStack getIcon();

  public abstract void giveTo(PSSPlayer buyer, int amount);

  public abstract boolean has(PSSPlayer seller, int amount);

  public abstract void removeFrom(PSSPlayer seller, int amount);

  public abstract void openEditor(Player player, AdminShop from);

}
