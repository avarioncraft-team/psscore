package de.projectsmallscope.psscore.modules.adminshops;

import de.projectsmallscope.psscore.util.Msg;
import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 12.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class AdminShop {

  public AdminShop(final String internalName, final String uiName) {
    this.adminShopComponentList = new ArrayList<>();
    this.internalName = internalName;
    this.uiName = uiName;
  }

  @Getter(AccessLevel.PROTECTED)
  private final List<AdminShopComponent> adminShopComponentList;
  @Getter
  private final String internalName;
  @Getter
  private final String uiName;
  @Getter
  @Setter
  private boolean locked = false;

  public void openEditor(final Player player) {
    AdminShopEditGUI.open(player, this);
  }

  public void openCustomerGUI(final Player player) {
    if (this.locked) {
      Msg.send(player, "Shop", "Dieser Shop ist gerade gesperrt.");
      return;
    }
    AdminShopCustomerGUI.open(player, this);
  }

}
