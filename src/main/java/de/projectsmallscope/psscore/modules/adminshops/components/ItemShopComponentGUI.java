package de.projectsmallscope.psscore.modules.adminshops.components;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.adminshops.AdminShop;
import de.projectsmallscope.psscore.modules.economy.EconomyModule;
import de.projectsmallscope.psscore.resourcepack.skins.Model;
import de.projectsmallscope.psscore.util.items.ItemBuilder;
import lombok.RequiredArgsConstructor;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.anvil.AnvilGUI;
import net.crytec.inventoryapi.anvil.Response;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 13.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class ItemShopComponentGUI implements InventoryProvider {

  public static void open(final Player player, final ItemShopComponent component, final AdminShop from) {
    SmartInventory.builder()
        .title("Editiere: " + component.getName())
        .size(5)
        .provider(new ItemShopComponentGUI(from, component))
        .build()
        .open(player);
  }

  private final AdminShop from;
  private final ItemShopComponent component;

  @Override
  public void init(final Player player, final InventoryContent content) {

    content.set(SlotPos.of(1, 4), ClickableItem.empty(this.component.getIcon()));
    content.set(SlotPos.of(4, 0), ClickableItem.of(new ItemBuilder(Model.BLACK_ARROW_LEFT.getItem()).name("§eZurück").build(), event -> {
      this.from.openEditor(player);
    }));
    final EconomyModule economyModule = PSSCore.getModule(EconomyModule.class);
    final ItemStack moneyIcon = new ItemBuilder(economyModule.getMoneyIcon(this.component.getBaseValue())).name("§6Basispreis setzen")
        .lore("", "§eMomentan: " + EconomyModule.formatString(this.component.getBaseValue(), false)).build();
    content.set(SlotPos.of(4, 0), ClickableItem.of(moneyIcon, event -> {
      new AnvilGUI.Builder().item(moneyIcon).title("Preis eingeben").onComplete((pl, in) -> {
        try {
          double num = Double.parseDouble(in);
          num = EconomyModule.round(num);
          this.component.setBaseValue(num);
        } catch (final NumberFormatException ignored) {

        }
        this.component.openEditor(pl, this.from);
        return Response.close();
      });
      this.from.openEditor(player);
    }));

  }

  @Override
  public void onClose(final Player player, final InventoryContent content) {
    if (this.from != null) {
      this.from.openEditor(player);
    }
  }

  @Override
  public void onBottomClick(final InventoryClickEvent event) {
    final ItemStack item = event.getCurrentItem();
    if (item == null || item.getType() == Material.AIR) {
      return;
    }
    this.component.setItemStack(item);
    this.reopen((Player) event.getWhoClicked());
  }

}
