package de.projectsmallscope.psscore.modules;

import de.projectsmallscope.psscore.PSSCore;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public interface BaseModule {

  void enable(PSSCore plugin);

  void disable(PSSCore plugin);

}