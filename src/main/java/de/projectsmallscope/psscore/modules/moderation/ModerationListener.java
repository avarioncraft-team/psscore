package de.projectsmallscope.psscore.modules.moderation;

import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import java.util.Optional;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class ModerationListener implements Listener {

  @EventHandler(priority = EventPriority.LOWEST)
  public void onChat(final AsyncPlayerChatEvent event) {
    final Player player = event.getPlayer();
    final PSSPlayer pssPlayer = PSSPlayer.of(player);
    final Optional<ModerationEviction> eviction = pssPlayer.getModerationGate().getChatEviction();
    eviction.ifPresent(restriction -> {
      UtilPlayer.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 0.6F, 0.8F);
      pssPlayer.sendMessage("Chat", "Deine Chatsperre dauert noch: " + Msg.elem(restriction.getFormattedTimeLeft()));
      event.setCancelled(true);
    });
  }

}
