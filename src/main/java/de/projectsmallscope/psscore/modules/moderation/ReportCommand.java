package de.projectsmallscope.psscore.modules.moderation;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("report|melden")
public class ReportCommand extends BaseCommand {

  @Default
  public void onReport(final Player sender) {
    // TODO open GUI
  }

  @Default
  @Syntax("<Spieler> <Grund>")
  @CommandCompletion("@CachedPlayer")
  public CompletableFuture<?> onReport(final Player sender, @Values("@CachedPlayer") final String target, final String reason) {
    return PSSCore.getModule(PlayerDataModule.class).getDataByName(target).thenApply(Optional::orElseThrow).thenAccept(pssPlayer -> {
      pssPlayer.getModerationGate().report(sender.getUniqueId(), sender.getName(), reason, sender.getLocation());
      final String nameEl = Msg.elem(pssPlayer.getLastSeenName());
      final String reasonEl = Msg.elem(reason);
      final PSSPlayer pssSender = PSSPlayer.of(sender);
      pssSender.sendMessage("Report", "Du hast " + nameEl + " für folgendes reportet:");
      pssSender.sendMessage("Report", reasonEl);
      pssSender.sendMessage("Report", "Schaue bei Reports am besten direkt auf den Tatort!");
    });
  }

}
