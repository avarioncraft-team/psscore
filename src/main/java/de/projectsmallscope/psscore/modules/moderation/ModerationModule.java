package de.projectsmallscope.psscore.modules.moderation;

import com.google.common.collect.ImmutableList;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.util.common.UtilTime;
import java.time.Duration;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class ModerationModule implements BaseModule {

  @Override
  public void enable(final PSSCore plugin) {
    PSSCore.registerListener(new ModerationListener());

    plugin.getPaperCommandManager().getCommandCompletions()
        .registerStaticCompletion("Duration", ImmutableList.of("15m", "30m", "3h:30m", "1d:1h", "1w:10m"));

    plugin.getPaperCommandManager().getCommandContexts()
        .registerContext(Duration.class, context -> UtilTime.toDuration(context.popFirstArg()));
    plugin.getPaperCommandManager().registerCommand(new ReportCommand());
    plugin.getPaperCommandManager().registerCommand(new BanCommand());
    plugin.getPaperCommandManager().registerCommand(new PermBanCommand());
    plugin.getPaperCommandManager().registerCommand(new MuteCommand());
    plugin.getPaperCommandManager().registerCommand(new WarnCommand());
  }

  @Override
  public void disable(final PSSCore plugin) {

  }
}
