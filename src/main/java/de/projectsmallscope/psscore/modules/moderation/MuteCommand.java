package de.projectsmallscope.psscore.modules.moderation;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilTime;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("mute")
@CommandPermission("moderator")
public class MuteCommand extends BaseCommand {

  @Default
  public void onMute(final Player sender) {
    // TODO open GUI
  }

  @Default
  @Syntax("<Spieler> <Grund>")
  @CommandCompletion("@CachedPlayer @Duration")
  public CompletableFuture<?> onMute(final Player sender, @Values("@CachedPlayer") final String target, final Duration duration,
      final String reason) {
    return PSSCore.getModule(PlayerDataModule.class).getDataByName(target).thenApply(Optional::orElseThrow).thenAccept(pssPlayer -> {
      pssPlayer.getModerationGate().restrictChat(duration, sender.getName(), reason);
      final String nameEl = Msg.elem(target);
      final String timeEl = Msg.elem(UtilTime.format(duration));
      PSSPlayer.of(sender).sendMessage("Chat", "Du hast " + nameEl + " für " + timeEl + " gemutet.");
    });
  }

}
