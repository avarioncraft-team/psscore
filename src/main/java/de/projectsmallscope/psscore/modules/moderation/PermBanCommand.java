package de.projectsmallscope.psscore.modules.moderation;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("permban")
@CommandPermission("command.permban")
public class PermBanCommand extends BaseCommand {

  @Default
  @Syntax("<Spieler> <Grund>")
  @CommandCompletion("@CachedPlayer")
  public CompletableFuture<?> onBan(final Player sender, @Values("@CachedPlayer") final String target, final String reason) {
    return PSSCore.getModule(PlayerDataModule.class).getDataByName(target).thenApply(Optional::orElseThrow).thenAccept(pssPlayer -> {
      pssPlayer.getModerationGate().permBan(sender.getName(), reason);

      final String nameEl = Msg.elem(pssPlayer.getLastSeenName());
      final String reasonEl = Msg.elem(reason);
      final String whoEl = Msg.elem(pssPlayer.getLastSeenName());

      PSSPlayer.of(sender).sendMessage("Ban", "Du hast " + nameEl + " gebannt.");
      PSSCore.getModule(PlayerDataModule.class).forEachOnline(online -> {
        online.getOnlinePlayer().ifPresent(pl -> UtilPlayer.playSound(pl, Sound.ENTITY_LIGHTNING_BOLT_THUNDER, 0.5F, 0.5F));
        online.getOnlinePlayer().ifPresent(pl -> UtilPlayer.playSound(pl, Sound.ENTITY_LIGHTNING_BOLT_THUNDER, 1.5F, 0.9F));
        online.sendMessage("Ban", whoEl + " wurde permanent gebannt.");
        online.sendMessage("Ban", "Grund: " + reasonEl);
      });
    });
  }

}
