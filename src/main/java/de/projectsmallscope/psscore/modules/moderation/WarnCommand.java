package de.projectsmallscope.psscore.modules.moderation;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import co.aikar.commands.annotation.Values;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandPermission("moderator")
@CommandAlias("warn|verwarnen")
public class WarnCommand extends BaseCommand {

  @Default
  public void onWarn(final Player sender) {
    // TODO open GUI
  }

  @Default
  @Syntax("<Spieler> <Grund>")
  @CommandCompletion("@CachedPlayer")
  public CompletableFuture<?> onWarn(final Player sender, @Values("@CachedPlayer") final String target, final boolean kick,
      final String reason) {
    return PSSCore.getModule(PlayerDataModule.class).getDataByName(target).thenApply(Optional::orElseThrow).thenAccept(pssPlayer -> {
      pssPlayer.getModerationGate().warn(sender.getName(), reason, kick);
      final String nameEl = Msg.elem(target);
      final String countEl = Msg.elem(pssPlayer.getModerationGate().getWarnCount() + ".");
      final PSSPlayer pssSender = PSSPlayer.of(sender);
      pssSender.sendMessage("Verwarnung", "Du hast " + nameEl + " verwarnd.");
      pssSender.sendMessage("Verwarnung", "Das ist die " + countEl + " Verwarnung für diesen Spieler.");
    });
  }

}