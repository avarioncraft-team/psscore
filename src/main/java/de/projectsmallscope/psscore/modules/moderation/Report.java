package de.projectsmallscope.psscore.modules.moderation;

import java.util.UUID;
import lombok.Data;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@Data
public class Report {

  private final UUID whoID;
  private final String who;
  private final String reportMsg;
  private final String where;

}
