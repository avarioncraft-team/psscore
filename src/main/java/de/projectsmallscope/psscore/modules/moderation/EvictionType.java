package de.projectsmallscope.psscore.modules.moderation;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public enum EvictionType {

  CHAT,
  TEMP_BAN,
  PERM_BAN,
  WARNING,
  WARNING_KICK

}