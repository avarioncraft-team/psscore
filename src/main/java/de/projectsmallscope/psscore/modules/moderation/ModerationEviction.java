package de.projectsmallscope.psscore.modules.moderation;

import de.projectsmallscope.psscore.util.common.UtilTime;
import java.time.Duration;
import lombok.Data;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 06.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@Data
public class ModerationEviction {


  private final String who;
  private final String reason;
  private final long until;
  private final boolean permanent;
  private final long evictionTime;
  private final EvictionType evictionType;

  public boolean isOver() {
    return this.until < System.currentTimeMillis();
  }

  public String getFormattedTimeLeft() {
    return UtilTime.format(Duration.ofMillis(this.until - System.currentTimeMillis()));
  }

}
