package de.projectsmallscope.psscore.modules.moderation;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class WarnEviction extends ModerationEviction {

  public WarnEviction(final String who, final String reason, final boolean withKick) {
    super(who, reason, 0, false, System.currentTimeMillis(), withKick ? EvictionType.WARNING_KICK : EvictionType.WARNING);
  }
}
