package de.projectsmallscope.psscore.modules.moderation;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.playerdata.PlayerDataComponent;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilPlayer;
import de.projectsmallscope.psscore.util.common.UtilTime;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 06.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class ModerationGate extends PlayerDataComponent {

  public ModerationGate(final UUID playerID) {
    super(playerID);
  }

  private ModerationEviction chatEviction = null;
  private ModerationEviction banEviction = null;
  private final List<ModerationEviction> evictionHistory = new ArrayList<>();
  private final List<Report> reportHistory = new ArrayList<>();

  public void report(final UUID whoID, final String who, final String reason, final Location where) {
    final PlayerDataModule dataModule = PSSCore.getModule(PlayerDataModule.class);
    PSSPlayer.of(this.ownerID).thenAccept(owner -> {
      final String name = owner.getLastSeenName();
      Bukkit.getOnlinePlayers().forEach(online -> {
        if (online.hasPermission("moderator")) {
          UtilPlayer.playSound(online, Sound.BLOCK_NOTE_BLOCK_PLING, 0.6F, 1.2F);
          dataModule.getOnlineData(online).sendMessage("Report", who + " -> " + name);
          dataModule.getOnlineData(online).sendMessage("Report", reason);
        }
      });
    });

    this.reportHistory.add(new Report(whoID, who, reason, where.toString()));
  }

  public Optional<ModerationEviction> getChatEviction() {
    if (this.chatEviction != null && !this.chatEviction.isPermanent() && this.chatEviction.isOver()) {
      this.chatEviction = null;
    }
    return Optional.ofNullable(this.chatEviction);
  }

  public Optional<ModerationEviction> getBanEviction() {
    if (this.banEviction != null && !this.banEviction.isPermanent() && this.banEviction.isOver()) {
      this.banEviction = null;
    }
    return Optional.ofNullable(this.banEviction);
  }

  public List<ModerationEviction> getEvictionHistory() {
    return new ArrayList<>(this.evictionHistory);
  }

  public List<Report> getReportHistory() {
    return new ArrayList<>(this.reportHistory);
  }

  public String getRestrictionTime() {
    return this.chatEviction == null ? "---" : this.chatEviction.getFormattedTimeLeft();
  }

  public String getBanTime() {
    return this.banEviction == null ? "---" : this.banEviction.getFormattedTimeLeft();
  }

  public void restrictChat(final Duration duration, final String who, final String reason) {
    this.restrictChat(duration, false, who, reason);
  }

  public long getWarnCount() {
    return this.evictionHistory.stream().filter(ev -> ev.getEvictionType().toString().startsWith("WARN")).count();
  }

  public void warn(final String who, final String reason, final boolean kick) {
    this.evictionHistory.add(new WarnEviction(who, reason, kick));
    final CompletableFuture<PSSPlayer> pssFuture = PSSPlayer.of(this.ownerID);
    pssFuture.thenAccept(pssPlayer -> {
      final Optional<Player> optionalPlayer = pssPlayer.getOnlinePlayer();
      final String whoEl = Msg.elem(who);
      final String reasonEl = Msg.elem(reason);
      final String countEl = Msg.elem("" + this.getWarnCount() + ".");
      pssPlayer.sendMessage("Verwarnung", "Du wurdest von " + whoEl + " verwarnd.");
      pssPlayer.sendMessage("Verwarnung", "Grund: " + reasonEl);
      pssPlayer.sendMessage("Verwarnung", "Das ist deine " + countEl + " Verwarnung.");
      if (kick) {
        final String kickMsg =
            "Du wurdest von " + whoEl + "§r verwarnt.\nGrund: " + reasonEl + "§r\nDas ist deine " + countEl + "§r Verwarnung.";
        optionalPlayer.ifPresent(player -> player.kickPlayer(kickMsg));
      }
    });
  }

  public void restrictChat(Duration duration, final boolean append, final String who, final String reason) {
    if (this.banEviction != null && this.banEviction.isPermanent()) {
      throw new IllegalStateException("Perma banned player cant be chat restricted.");
    }
    if (append && this.chatEviction != null) {
      duration = Duration.ofMillis(duration.toMillis() + (this.chatEviction.getUntil() - System.currentTimeMillis()));
    }
    final long restrictedUntil = System.currentTimeMillis() + duration.toMillis();
    this.chatEviction = new ModerationEviction(who, reason, restrictedUntil, false, System.currentTimeMillis(), EvictionType.CHAT);
    this.evictionHistory.add(this.chatEviction);
    final String timeElement = Msg.elem(UtilTime.format(duration));
    PSSPlayer.of(this.ownerID).thenAccept(pss -> {
      pss.sendMessage("Chat", "Du bist jetzt für " + timeElement + " vom Chat ausgeschlossen.");
      pss.sendMessage("Chat", "Wer: " + Msg.elem(who));
      pss.sendMessage("Chat", "Warum: " + Msg.elem(reason));
    });
  }

  public void ban(final Duration duration, final String who, final String reason) {
    this.ban(duration, false, who, reason);
  }

  public void ban(Duration duration, final boolean append, final String who, final String reason) {
    if (this.banEviction != null && this.banEviction.isPermanent()) {
      throw new IllegalStateException("Perma banned player cant be banned again.");
    }
    if (append && this.banEviction != null) {
      duration = Duration.ofMillis(duration.toMillis() + (this.banEviction.getUntil() - System.currentTimeMillis()));
    }
    final long bannedUntil = System.currentTimeMillis() + duration.toMillis();
    this.banEviction = new ModerationEviction(who, reason, bannedUntil, false, System.currentTimeMillis(), EvictionType.TEMP_BAN);
    this.evictionHistory.add(this.banEviction);
    final String timeElement = Msg.elem(UtilTime.format(duration));
    final String whoEl = Msg.elem(who);
    final String reasonEl = Msg.elem(reason);
    PSSPlayer.of(this.ownerID).thenAccept(pss -> {
      final Optional<Player> playerOptional = pss.getOnlinePlayer();
      playerOptional.ifPresent(
          player -> player.kickPlayer("Du wurdest für " + timeElement + "§r gebannt. \nGrund: " + reasonEl + "§r\n" + "Wer: " + whoEl));
    });
  }

  public void permBan(final String who, final String reason) {
    if (this.banEviction != null && this.banEviction.isPermanent()) {
      throw new IllegalStateException("Perma banned player cant be banned again.");
    }

    this.banEviction = new ModerationEviction(who, reason, -1, true, System.currentTimeMillis(), EvictionType.PERM_BAN);
    this.evictionHistory.add(this.banEviction);
    final String whoEl = Msg.elem(who);
    final String reasonEl = Msg.elem(reason);
    PSSPlayer.of(this.ownerID).thenAccept(pss -> {
      final Optional<Player> playerOptional = pss.getOnlinePlayer();
      playerOptional
          .ifPresent(player -> player.kickPlayer("Du wurdest §cpermanent§r gebannt.\nGrund: " + reasonEl + "§r\n" + "Wer: " + whoEl));
    });
  }

}