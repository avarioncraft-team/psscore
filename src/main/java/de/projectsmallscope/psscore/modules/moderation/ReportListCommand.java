package de.projectsmallscope.psscore.modules.moderation;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Values;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 07.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@CommandAlias("listreports")
@CommandPermission("moderator")
public class ReportListCommand extends BaseCommand {

  @Default
  @CommandCompletion("@CachedPlayerAll")
  public void onList(final Player sender, @Values("@CachedPlayerAll") final String cachedPlayerName) {
    // TODO open
  }

}
