package de.projectsmallscope.psscore.modules.protection;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.io.PSSIO;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.modules.protection.RuleSet.RuleSetSerializer;
import de.projectsmallscope.psscore.modules.protection.json.ChunkDomainMapSerializer;
import de.projectsmallscope.psscore.modules.protection.json.RegionSetSerializer;
import de.projectsmallscope.psscore.playerdata.PSSPlayer;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.util.BoundingBox;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of AvarionCore and was created at the 30.06.2020
 *
 * AvarionCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class ProtectionModule implements BaseModule, Listener {

  public ProtectionModule() {
    final PSSCore plugin = PSSCore.getInstance();
    this.worlds = new Object2ObjectOpenHashMap<>();
    this.pssIO = PSSCore.getInstance().getPssIO();
    PSSCore.registerListener(this);
    PSSCore.registerListener(new ProtectionListener(this));
  }

  private final PSSIO pssIO;
  private final Object2ObjectMap<UUID, WorldDomain> worlds;

  public WorldDomain getWorldDomain(final UUID worldID) {
    return this.worlds.get(worldID);
  }

  public ProtectedRegion createRegion(final Location corner1, final Location corner2, final UUID ownerID, final int priority) {
    return this.createRegion(corner1.getBlock(), corner2.getBlock(), ownerID, priority);
  }

  public ProtectedRegion createRegion(final Block corner1, final Block corner2, final UUID ownerID, final int priority) {
    final BoundingBox box = BoundingBox.of(corner1, corner2);
    final ProtectedRegion region = new ProtectedRegion(box, ownerID, priority);
    this.worlds.get(corner1.getWorld().getUID()).addRegion(region);
    return region;
  }

  public ProtectedRegion getHighestPriorityRegionAt(final Block block) {
    return this.worlds.get(block.getWorld().getUID()).getHighestPriorityRegionAt(block);
  }

  public RuleSet getPlayerRules(final Block block, final PSSPlayer pssPlayer) {
    return this.worlds.get(block.getWorld().getUID()).getPlayerRules(block, pssPlayer);
  }

  public RuleSet getEnvironmentRules(final Block block) {
    return this.worlds.get(block.getWorld().getUID()).getEnvironmentRules(block);
  }

  private void loadWorld(final UUID worldID) {
    final Optional<String> data = this.pssIO.loadWorldProtectionData(worldID);
    final WorldDomain domain = data.isEmpty() ? new WorldDomain(worldID) : GsonProvider.fromJson(data.get(), WorldDomain.class);
    domain.forceUpdate();
    this.worlds.put(worldID, domain);
  }

  private void unloadWorld(final UUID worldID) {
    final WorldDomain domain = this.worlds.get(worldID);
    if (domain != null) {
      this.pssIO.saveWorldProtectionData(worldID, GsonProvider.toJson(domain));
      this.worlds.remove(worldID);
    }
  }

  @EventHandler
  public void onLoad(final WorldLoadEvent event) {
    this.loadWorld(event.getWorld().getUID());
  }

  @EventHandler
  public void onUnload(final WorldUnloadEvent event) {
    this.unloadWorld(event.getWorld().getUID());
  }

  public void flushData() {
    for (final UUID worldID : new HashSet<>(this.worlds.keySet())) {
      this.unloadWorld(worldID);
    }
  }

  @Override
  public void enable(final PSSCore plugin) {
    GsonProvider.register(RuleSet.class, new RuleSetSerializer());
    GsonProvider.register(RegionSet.class, new RegionSetSerializer());
    GsonProvider.register(ChunkDomain.class, new ChunkDomainMapSerializer());
    for (final World world : Bukkit.getWorlds()) {
      this.loadWorld(world.getUID());
    }
  }

  @Override
  public void disable(final PSSCore plugin) {
    this.flushData();
  }
}