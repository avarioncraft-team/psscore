package de.projectsmallscope.psscore.playerdata;

import com.google.common.base.Preconditions;
import de.projectsmallscope.psscore.modules.moderation.ModerationEviction;
import de.projectsmallscope.psscore.modules.moderation.ModerationGate;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import lombok.AllArgsConstructor;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@AllArgsConstructor
public class PlayerDataListener implements Listener {

  private final PlayerDataModule playerDataManager;

  @EventHandler
  public void onPreLogin(final AsyncPlayerPreLoginEvent event) {
    try {
      final PSSPlayer pssPlayer = Preconditions.checkNotNull(this.playerDataManager.getData(event.getUniqueId()).get());
      final ModerationGate gate = pssPlayer.getModerationGate();
      final Optional<ModerationEviction> eviction = gate.getBanEviction();
      eviction.ifPresent(ban -> {
        final String reason = Msg.elem(ban.getReason());
        event.disallow(Result.KICK_BANNED, "Du wurdest wegen " + reason + "§r gebannt.\n" +
            (ban.isPermanent() ? "§cDieser Ban ist permanent." : "Zeit verbleibend: " + Msg.elem(ban.getFormattedTimeLeft())));
      });
      this.playerDataManager.getPssIO().savePlayerJsonData(pssPlayer.getPlayerID(), GsonProvider.toJson(pssPlayer));
    } catch (final InterruptedException | ExecutionException e) {
      event.disallow(Result.KICK_OTHER, "Es ist ein Fehler beim Laden deiner Daten aufgetreten.");
      e.printStackTrace();
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void onJoin(final PlayerJoinEvent event) {
    final Player player = event.getPlayer();
    final PSSPlayer pssPlayer = this.playerDataManager.getOnlineData(player);

    pssPlayer.setLastSeenGameProfile(((CraftPlayer) player).getProfile());
    pssPlayer.setLastSeen(System.currentTimeMillis());
    pssPlayer.setLastDataAccess(System.currentTimeMillis());

    pssPlayer.applyCachesToPlayer();
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void onLeave(final PlayerQuitEvent event) {
    final ItemStack[] content = event.getPlayer().getInventory().getContents();
    this.playerDataManager.getData(event.getPlayer().getUniqueId()).thenAccept(pss -> pss.setInventoryContent(content));
  }

}