package de.projectsmallscope.psscore.playerdata;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import de.projectsmallscope.psscore.io.PSSIO;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@AllArgsConstructor
public class PlayerDataPersistenceListener implements RemovalListener<UUID, PSSPlayer>, CacheLoader<UUID, PSSPlayer> {

  private final PSSIO pssIO;

  @Override
  public void onRemoval(final UUID key, final PSSPlayer value, final RemovalCause cause) {
    System.out.println("§a>> Saving Player: " + key);
    final String json = GsonProvider.toJson(value);
    this.pssIO.savePlayerJsonDataAsync(key, json);
  }

  @Override
  public PSSPlayer load(final UUID key) {
    final Optional<String> optionalJson = this.pssIO.loadPlayerJsonData(key);
    System.out.println("§a>> Loading Player: " + key);
    return optionalJson.isPresent() ? GsonProvider.fromJson(optionalJson.get(), PSSPlayer.class) : new PSSPlayer(key);
  }
}