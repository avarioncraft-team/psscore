package de.projectsmallscope.psscore.playerdata;

import co.aikar.commands.PaperCommandManager;
import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Scheduler;
import com.google.common.collect.Multimap;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.io.PSSConfiguration;
import de.projectsmallscope.psscore.io.PSSIO;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.playerdata.subdata.MultimapSerializer;
import de.projectsmallscope.psscore.util.json.GsonProvider;
import de.projectsmallscope.psscore.util.tasks.TaskManager;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class PlayerDataModule implements BaseModule {

  @Getter(AccessLevel.PROTECTED)
  private final PSSIO pssIO;
  private final TaskManager taskManager;
  private final AsyncLoadingCache<UUID, PSSPlayer> playerCache;

  public PlayerDataModule(final PSSIO pssIO) {
    this.taskManager = TaskManager.getInstance();
    this.pssIO = pssIO;
    final PSSConfiguration configuration = pssIO.loadConfiguration();
    final Duration cacheDuration = configuration.getPlayerCacheDuration();

    GsonProvider.register(Multimap.class, new MultimapSerializer());

    final PlayerDataPersistenceListener persistenceListener = new PlayerDataPersistenceListener(pssIO);
    this.playerCache = Caffeine.newBuilder()
        .removalListener(persistenceListener)
        .expireAfterAccess(cacheDuration)
        .scheduler(Scheduler.forScheduledExecutorService(this.taskManager.getScheduler()))
        .buildAsync(persistenceListener);

    PSSCore.registerListener(new PlayerDataListener(this));
    TaskManager.getInstance().runRepeatedBukkit(this::refreshOnlineTimers, 0, 20);
  }

  public CompletableFuture<Optional<PSSPlayer>> getDataByName(final String name) {
    final Player player = Bukkit.getPlayer(name);
    if (player != null) {
      return CompletableFuture.completedFuture(Optional.of(this.getOnlineData(player)));
    }
    return CompletableFuture.supplyAsync(() -> this.loadByName(name));
  }

  public PSSPlayer getOnlineData(final UUID playerID) {
    return this.playerCache.synchronous().get(playerID);
  }

  public PSSPlayer getOnlineData(final Player player) {
    return player == null ? null : this.playerCache.synchronous().get(player.getUniqueId());
  }

  public CompletableFuture<PSSPlayer> getData(final UUID playerID) {
    return this.playerCache.get(playerID).thenApply(data -> {
      data.setLastDataAccess(System.currentTimeMillis());
      return data;
    });
  }

  private Optional<PSSPlayer> loadByName(final String name) {
    final Optional<String> data = this.pssIO.loadPlayerJsonDataByName(name);
    if (data.isEmpty()) {
      return Optional.empty();
    }
    return Optional.ofNullable(GsonProvider.fromJson(data.get(), PSSPlayer.class));
  }

  private void refreshOnlineTimers() {
    Bukkit.getOnlinePlayers().forEach(this::getOnlineData);
  }

  public CompletableFuture<Void> bukkitSyncInteract(final UUID invID, final UUID recID, final BiConsumer<PSSPlayer, PSSPlayer> consumer) {
    final CompletableFuture<PSSPlayer> senderFuture = this.getData(invID);
    final CompletableFuture<PSSPlayer> receiverFuture = this.getData(recID);
    return senderFuture.thenAcceptBoth(receiverFuture, this.taskManager.biConsumeBukkitSync(consumer));
  }

  public CompletableFuture<Void> bukkitSync(final UUID playerID, final Consumer<PSSPlayer> consumer) {
    return this.getData(playerID).thenAccept(this.taskManager.consumeBukkitSync(consumer));
  }

  public void forEachOnline(final Consumer<PSSPlayer> pssPlayerConsumer) {
    Bukkit.getOnlinePlayers().forEach(online -> pssPlayerConsumer.accept(this.getOnlineData(online)));
  }

  @Override
  public void enable(final PSSCore plugin) {
    final PaperCommandManager commandManager = plugin.getPaperCommandManager();
    commandManager.getCommandCompletions().registerCompletion("CachedPlayer",
        input -> this.playerCache.synchronous().asMap().values().stream().map(PSSPlayer::getLastSeenName).collect(Collectors.toList()));
    commandManager.getCommandCompletions().registerAsyncCompletion("CachedPlayerAll",
        input -> this.pssIO.listAllRelevantPlayerNames());
  }

  @Override
  public void disable(final PSSCore plugin) {
    this.playerCache.asMap().clear();
  }


}