package de.projectsmallscope.psscore.playerdata;

import de.projectsmallscope.psscore.PSSCore;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 09.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
@RequiredArgsConstructor
public class PlayerDataComponent {

  protected final UUID ownerID;

  public Optional<Player> getPlayer() {
    return Optional.ofNullable(Bukkit.getPlayer(this.ownerID));
  }

  public PSSPlayer getHost() {
    return PSSCore.getModule(PlayerDataModule.class).getOnlineData(this.ownerID);
  }

}
