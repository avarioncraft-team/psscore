package de.projectsmallscope.psscore.playerdata;

import com.google.gson.annotations.SerializedName;
import com.mojang.authlib.GameProfile;
import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.economy.EconomyAccount;
import de.projectsmallscope.psscore.modules.friends.FriendAccount;
import de.projectsmallscope.psscore.modules.friends.FriendsGUI;
import de.projectsmallscope.psscore.modules.moderation.ModerationGate;
import de.projectsmallscope.psscore.modules.trade.TradeRequestIntermediary;
import de.projectsmallscope.psscore.util.Msg;
import de.projectsmallscope.psscore.util.common.UtilItem;
import de.projectsmallscope.psscore.util.guis.ConfirmationGUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class PSSPlayer {

  @SerializedName("CachedMessages")
  private final List<Msg.Pack> cachedMessages;

  @SerializedName("CachedItems")
  private final List<ItemStack> cachedItemStacks;

  @Getter
  @Setter(AccessLevel.PROTECTED)
  @SerializedName("InventoryContent")
  private ItemStack[] inventoryContent;

  @Getter
  @SerializedName("PlayerUUID")
  private final UUID playerID;

  @Getter
  @SerializedName("FriendAccount")
  private final FriendAccount friendAccount;

  @Getter
  @Setter(AccessLevel.PROTECTED)
  @SerializedName("LastSeenGameProfile")
  private GameProfile lastSeenGameProfile;

  @Getter
  @Setter(AccessLevel.PROTECTED)
  @SerializedName("LastSeen")
  private long lastSeen;

  @Getter
  @Setter(AccessLevel.PROTECTED)
  @SerializedName("LastDataAccess")
  private long lastDataAccess;

  @Getter
  @SerializedName("EconomyAccount")
  private final EconomyAccount economyAccount;

  @Getter
  @Setter(AccessLevel.PROTECTED)
  @SerializedName("ModerationGate")
  private ModerationGate moderationGate;

  @Getter
  @SerializedName("TradeRequestIntermediary")
  private final TradeRequestIntermediary tradeRequestIntermediary;

  @Getter
  @SerializedName("Settings")
  private final PlayerSettings playerSettings;

  private transient ItemStack lastSeenHead;

  public static CompletableFuture<PSSPlayer> of(final UUID playerID) {
    return PSSCore.getModule(PlayerDataModule.class).getData(playerID);
  }

  public static PSSPlayer of(final Player player) {
    return PSSCore.getModule(PlayerDataModule.class).getOnlineData(player);
  }

  public PSSPlayer(final UUID playerID) {
    this.playerID = playerID;
    this.cachedMessages = new ArrayList<>();
    this.cachedItemStacks = new ArrayList<>();
    this.friendAccount = new FriendAccount(playerID);
    this.moderationGate = new ModerationGate(playerID);
    this.economyAccount = new EconomyAccount(playerID);
    this.playerSettings = new PlayerSettings(playerID);
    this.tradeRequestIntermediary = new TradeRequestIntermediary(playerID);
  }

  public ItemStack getLastSeenHead() {
    if (this.lastSeenHead == null) {
      this.lastSeenHead = UtilItem.produceHead(this.lastSeenGameProfile);
    }
    return this.lastSeenHead;
  }

  public void addItem(final ItemStack item) {
    this.getOnlinePlayer().ifPresentOrElse(online -> online.getInventory()
            .addItem(item)
            .values()
            .forEach(left -> online.getWorld().dropItemNaturally(online.getLocation(), left))
        , () -> this.cachedItemStacks.add(item));
  }

  public String getLastSeenName() {
    return this.lastSeenGameProfile.getName();
  }

  public Optional<Player> getOnlinePlayer() {
    return Optional.ofNullable(Bukkit.getPlayer(this.playerID));
  }

  public void sendMessage(final String prefix, final Object message) {
    final Optional<Player> opPlayer = this.getOnlinePlayer();
    if (opPlayer.isPresent()) {
      Msg.send(opPlayer.get(), prefix, message.toString());
    } else {
      this.cachedMessages.add(new Msg.Pack(prefix, message.toString()));
    }
  }

  public void openFriendsGUI() {
    final Optional<Player> player = this.getOnlinePlayer();
    player.ifPresent(FriendsGUI::open);
  }

  public void applyCachesToPlayer() {
    final Optional<Player> opPlayer = this.getOnlinePlayer();
    if (opPlayer.isEmpty()) {
      return;
    }
    final Player player = opPlayer.get();
    for (final Msg.Pack pack : this.cachedMessages) {
      Msg.send(player, pack.getModuleName(), pack.getMessage());
    }
    if (!this.cachedItemStacks.isEmpty()) {
      ConfirmationGUI.open(player, ok -> {
        if (ok) {
          for (final ItemStack itemStack : this.cachedItemStacks) {
            player.getInventory().addItem(itemStack).values()
                .forEach(left -> player.getWorld().dropItemNaturally(player.getLocation(), left));
          }
        }
      }, "Du hast neue Items ehalten. Jetzt aufsammeln?");
    }
    this.cachedMessages.clear();
    this.cachedItemStacks.clear();
    if (this.inventoryContent != null) {
      player.getInventory().setContents(this.inventoryContent);
    }
  }

}