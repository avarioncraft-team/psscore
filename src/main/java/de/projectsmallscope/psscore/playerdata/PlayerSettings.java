package de.projectsmallscope.psscore.playerdata;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 08.11.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class PlayerSettings extends PlayerDataComponent {

  public PlayerSettings(UUID ownerID) {
    super(ownerID);
  }

  @Getter
  @Setter
  private boolean notifyOnMoneyPickup = true;
  
}
