package de.projectsmallscope.psscore;

import co.aikar.commands.PaperCommandManager;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.projectsmallscope.psscore.io.MongoIO;
import de.projectsmallscope.psscore.io.PSSConfiguration;
import de.projectsmallscope.psscore.io.PSSIO;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.modules.economy.EconomyModule;
import de.projectsmallscope.psscore.modules.friends.FriendAccountModule;
import de.projectsmallscope.psscore.modules.moderation.ModerationModule;
import de.projectsmallscope.psscore.modules.protection.ProtectionModule;
import de.projectsmallscope.psscore.modules.trade.TradeModule;
import de.projectsmallscope.psscore.playerdata.PlayerDataModule;
import de.projectsmallscope.psscore.resourcepack.ResourcepackModule;
import de.projectsmallscope.psscore.util.DebugCommand;
import de.projectsmallscope.psscore.util.UtilModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class PSSCore extends JavaPlugin {

  @Getter
  private static PSSCore instance;

  @Getter
  private ProtocolManager protocolManager;
  @Getter
  private PSSIO pssIO;
  @Getter
  private PaperCommandManager paperCommandManager;

  private final LinkedHashMap<Class<? extends BaseModule>, BaseModule> moduleMap = new LinkedHashMap<>();

  public static <T extends BaseModule> T getModule(final Class<T> clazz) {
    return clazz.cast(PSSCore.instance.moduleMap.get(clazz));
  }

  public PSSConfiguration getPSSConfig() {
    return this.pssIO.loadConfiguration();
  }

  public static void registerListener(final Listener listener) {
    Bukkit.getPluginManager().registerEvents(listener, PSSCore.instance);
  }

  @Override
  public void onEnable() {
    PSSCore.instance = this;
    this.paperCommandManager = new PaperCommandManager(this);
    this.protocolManager = ProtocolLibrary.getProtocolManager();
    this.pssIO = new MongoIO(this);

    this.moduleMap.put(UtilModule.class, new UtilModule());
    this.moduleMap.put(ResourcepackModule.class, new ResourcepackModule());
    this.moduleMap.put(PlayerDataModule.class, new PlayerDataModule(this.pssIO));
    this.moduleMap.put(FriendAccountModule.class, new FriendAccountModule());
    this.moduleMap.put(ProtectionModule.class, new ProtectionModule());
    this.moduleMap.put(ModerationModule.class, new ModerationModule());
    this.moduleMap.put(EconomyModule.class, new EconomyModule());
    this.moduleMap.put(TradeModule.class, new TradeModule());

    this.enableModules();

    this.paperCommandManager.registerCommand(new DebugCommand());
  }

  @Override
  public void onDisable() {
    this.disableModules();
  }

  private void enableModules() {
    for (final BaseModule module : this.moduleMap.values()) {
      try {
        module.enable(this);
      } catch (final Exception e) {
        this.getLogger().severe("Could not enable module: " + module.getClass().getName());
        e.printStackTrace();
        this.getLogger().severe("Shutting down server");
        Bukkit.shutdown();
      }
    }
  }

  private void disableModules() {
    final ArrayList<BaseModule> modules = new ArrayList<>(this.moduleMap.values());
    Collections.reverse(modules);
    modules.forEach(module -> module.disable(this));
  }

}
