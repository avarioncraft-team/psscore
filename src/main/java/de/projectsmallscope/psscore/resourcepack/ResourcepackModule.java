package de.projectsmallscope.psscore.resourcepack;

import de.projectsmallscope.psscore.PSSCore;
import de.projectsmallscope.psscore.modules.BaseModule;
import de.projectsmallscope.psscore.resourcepack.distribution.ResourcepackListener;
import de.projectsmallscope.psscore.resourcepack.distribution.ResourcepackManager;
import de.projectsmallscope.psscore.resourcepack.packing.ResourcepackAssembler;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import org.bukkit.Bukkit;

public class ResourcepackModule implements BaseModule {

  private ResourcepackManager resourcepackManager;

  @Override
  public void enable(final PSSCore plugin) {
    CompletableFuture.runAsync(() -> {
      try {
        new ResourcepackAssembler(plugin).zipResourcepack();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    }).thenRun(() -> {
      try {
        this.resourcepackManager = new ResourcepackManager();
      } catch (final Exception exception) {
        exception.printStackTrace();
        Bukkit.shutdown();
      }
      PSSCore.registerListener(new ResourcepackListener(plugin, this.resourcepackManager));
    });
  }

  @Override
  public void disable(final PSSCore plugin) {
    if (this.resourcepackManager == null) {
      System.out.println("§c ResourcepackManager is null.");
    } else {
      this.resourcepackManager.shutdown();
    }
  }
}
