package de.projectsmallscope.psscore.io;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 22.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public interface PSSIO {

  void savePlayerJsonData(UUID playerID, String json);

  Optional<String> loadPlayerJsonData(UUID playerID);

  Optional<String> loadPlayerJsonDataByName(String playerName);

  void saveWorldProtectionData(UUID worldID, String data);

  Optional<String> loadWorldProtectionData(UUID worldID);

  PSSConfiguration loadConfiguration();

  List<String> listAllRelevantPlayerNames();

  Optional<String> loadAdminShopData();

  void saveAdminShopData(String data);

  default void savePlayerJsonDataAsync(final UUID playerID, final String json) {
    CompletableFuture.runAsync(() -> this.savePlayerJsonData(playerID, json));
  }

  default CompletableFuture<Optional<String>> loadPlayerJsonDataAsync(final UUID playerID) {
    return CompletableFuture.supplyAsync(() -> this.loadPlayerJsonData(playerID));
  }

}