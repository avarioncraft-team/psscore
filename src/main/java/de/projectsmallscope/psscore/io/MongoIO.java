package de.projectsmallscope.psscore.io;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.Indexes;
import de.projectsmallscope.psscore.util.functional.CachedAccess;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MongoIO implements PSSIO {

  private static final String PLAYER_COLLECTION_NAME = "Players";
  private static final String WORLD_DOMAIN_COLLECTION_NAME = "WorldDomains";
  private static final String ADMIN_SHOP_COLLECTION_NAME = "AdminShops";

  private final CachedAccess<List<String>> relevantPlayerNamesCache;
  private final MongoDatabase database;
  private final MongoCollection<Document> playerCollection;
  private final MongoCollection<Document> worldDomainCollection;
  private final MongoCollection<Document> adminShopCollection;
  private final JavaPlugin plugin;
  private PSSConfiguration config;

  public MongoIO(final JavaPlugin plugin) {
    this.plugin = plugin;
    final PSSConfiguration configuration = this.loadConfiguration();
    this.relevantPlayerNamesCache = new CachedAccess<>(configuration.getNameCacheDuration(), this::loadAllRelevantPlayerNames);
    final MongoClientURI uri = new MongoClientURI(this.config.getMongoURI());
    final MongoClient client = new MongoClient(uri);
    this.database = client.getDatabase(this.config.getDBName());

    if (!this.collectionExists(MongoIO.PLAYER_COLLECTION_NAME)) {
      this.database.createCollection(MongoIO.PLAYER_COLLECTION_NAME);
      this.database.getCollection(MongoIO.PLAYER_COLLECTION_NAME).createIndex(Indexes.hashed("PlayerUUID"));
      this.database.getCollection(MongoIO.PLAYER_COLLECTION_NAME).createIndex(Indexes.hashed("LastSeenGameProfile.name"));
    }

    if (!this.collectionExists(MongoIO.WORLD_DOMAIN_COLLECTION_NAME)) {
      this.database.createCollection(MongoIO.WORLD_DOMAIN_COLLECTION_NAME);
      this.database.getCollection(MongoIO.WORLD_DOMAIN_COLLECTION_NAME).createIndex(Indexes.hashed("WorldUUID"));
    }

    if (!this.collectionExists(MongoIO.ADMIN_SHOP_COLLECTION_NAME)) {
      this.database.createCollection(MongoIO.ADMIN_SHOP_COLLECTION_NAME);
    }

    this.playerCollection = this.database.getCollection(MongoIO.PLAYER_COLLECTION_NAME);
    this.worldDomainCollection = this.database.getCollection(MongoIO.WORLD_DOMAIN_COLLECTION_NAME);
    this.adminShopCollection = this.database.getCollection(MongoIO.ADMIN_SHOP_COLLECTION_NAME);
  }

  /**
   * Updates a player in the DB If the player does not exist he is inserted. This is provided my the "upsert" functionality
   *
   * @param playerID the playerID
   * @param json     the player data
   */
  @Override
  public void savePlayerJsonData(final UUID playerID, final String json) {
    final Document playerDocument = Document.parse(json);
    final Bson filter = Filters.eq("PlayerUUID", playerID.toString());
    final FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().upsert(true);
    try {
      this.playerCollection.findOneAndReplace(filter, playerDocument, options);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Finds a players data in the data base
   *
   * @param playerID the players UUID
   * @return the data as json String or null if not present (As Optional)
   */
  @Override
  public Optional<String> loadPlayerJsonData(final UUID playerID) {
    final Document playerDocument = this.playerCollection.find(Filters.eq("PlayerUUID", playerID.toString())).first();
    final JsonWriterSettings jsonSettings = JsonWriterSettings.builder()
        .int64Converter((value, writer) -> writer.writeNumber(value.toString()))
        .build();
    return Optional.ofNullable(playerDocument == null ? null : playerDocument.toJson(jsonSettings));
  }

  @Override
  public Optional<String> loadPlayerJsonDataByName(final String playerName) {
    final Document playerDocument = this.playerCollection.find(Filters.eq("LastSeenGameProfile.name", playerName)).first();
    final JsonWriterSettings jsonSettings = JsonWriterSettings.builder()
        .int64Converter((value, writer) -> writer.writeNumber(value.toString()))
        .build();
    return Optional.ofNullable(playerDocument == null ? null : playerDocument.toJson(jsonSettings));
  }

  @Override
  public void saveWorldProtectionData(final UUID worldID, final String data) {
    final Document worldDocument = Document.parse(data);
    final Bson filter = Filters.eq("WorldUUID", worldID.toString());
    final FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().upsert(true);
    try {
      this.worldDomainCollection.findOneAndReplace(filter, worldDocument, options);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Optional<String> loadWorldProtectionData(final UUID worldID) {
    final Document worldDocument = this.worldDomainCollection.find(Filters.eq("WorldUUID", worldID.toString())).first();
    final JsonWriterSettings jsonSettings = JsonWriterSettings.builder()
        .int64Converter((value, writer) -> writer.writeNumber(value.toString()))
        .build();
    return Optional.ofNullable(worldDocument == null ? null : worldDocument.toJson(jsonSettings));
  }

  @Override
  public PSSConfiguration loadConfiguration() {
    if (this.config == null) {
      this.config = new PSSFileConfiguration(Objects.requireNonNull(this.loadOrCreateConfig()));
    }
    return this.config;
  }

  @Override
  public List<String> listAllRelevantPlayerNames() {
    return this.relevantPlayerNamesCache.getValue();
  }

  @Override
  public Optional<String> loadAdminShopData() {
    final Document shopManagerDocument = this.adminShopCollection.find().first();
    final JsonWriterSettings jsonSettings = JsonWriterSettings.builder()
        .int64Converter((value, writer) -> writer.writeNumber(value.toString()))
        .build();
    return Optional.ofNullable(shopManagerDocument == null ? null : shopManagerDocument.toJson(jsonSettings));
  }

  @Override
  public void saveAdminShopData(final String data) {
    final Document adminShopDocument = Document.parse(data);
    this.worldDomainCollection.replaceOne(new Document(), adminShopDocument);
  }

  private List<String> loadAllRelevantPlayerNames() {
    final Set<String> names = new HashSet<>();
    this.playerCollection.distinct("LastSeenGameProfile.name", String.class).into(names);
    for (final Player player : Bukkit.getOnlinePlayers()) {
      names.add(player.getName());
    }
    return new ArrayList<>(names);
  }

  private FileConfiguration loadOrCreateConfig() {
    if (this.plugin == null) {
      return null;
    }
    final File pluginFolder = this.plugin.getDataFolder();
    pluginFolder.mkdirs();
    final File configFile = new File(pluginFolder, "config.yml");
    if (!configFile.exists()) {
      this.plugin.saveResource("config.yml", false);
    }
    return YamlConfiguration.loadConfiguration(configFile);
  }

  private boolean collectionExists(final String name) {
    for (final String cName : this.database.listCollectionNames()) {
      if (cName.equals(name)) {
        return true;
      }
    }
    return false;
  }

}