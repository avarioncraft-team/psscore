package de.projectsmallscope.psscore.io;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.bukkit.configuration.file.FileConfiguration;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 23.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */
public class PSSFileConfiguration implements PSSConfiguration {

  protected PSSFileConfiguration(final FileConfiguration fileConfiguration) {
    this.mongoURI = fileConfiguration.getString("MongoURI");
    this.dbName = fileConfiguration.getString("DBName");
    final TimeUnit playerCacheUnit = TimeUnit.valueOf(fileConfiguration.getString("PlayerCache.TimeUnit"));
    final int playerCacheValue = fileConfiguration.getInt("PlayerCache.Value");
    this.cacheDuration = Duration.of(playerCacheValue, playerCacheUnit.toChronoUnit());
    this.resourcePackServerIP = fileConfiguration.getString("ResourcepackServer.IP");
    this.resourcePackServerPort = fileConfiguration.getInt("ResourcepackServer.PORT");
    this.threadPoolSize = fileConfiguration.getInt("SchedulerThreadPoolSize", 2);
    final TimeUnit playerNameCacheUnit = TimeUnit.valueOf(fileConfiguration.getString("PlayerNameCache.TimeUnit"));
    final int playerNameCacheValue = fileConfiguration.getInt("PlayerNameCache.Value");
    this.nameCacheDuration = Duration.of(playerNameCacheValue, playerNameCacheUnit.toChronoUnit());
    this.moneyNameSingular = fileConfiguration.getString("Economy.NameSingular");
    this.moneyNamePlural = fileConfiguration.getString("Economy.NamePlural");
    this.moneyCombinationRadius = fileConfiguration.getDouble("Economy.CombinationRadius");
    this.adminShopInterestRate = fileConfiguration.getDouble("AdminShops.InterestRate", 1.001);
  }

  private final String mongoURI;
  private final String dbName;
  private final Duration cacheDuration;
  private final String resourcePackServerIP;
  private final int resourcePackServerPort;
  private final int threadPoolSize;
  private final Duration nameCacheDuration;
  private final String moneyNameSingular;
  private final String moneyNamePlural;
  private final double moneyCombinationRadius;
  private final double adminShopInterestRate;

  @Override
  public String getMongoURI() {
    return this.mongoURI;
  }

  @Override
  public String getDBName() {
    return this.dbName;
  }

  @Override
  public String getResourcePackServerIP() {
    return this.resourcePackServerIP;
  }

  @Override
  public int getResourcePackServerPort() {
    return this.resourcePackServerPort;
  }

  @Override
  public Duration getPlayerCacheDuration() {
    return this.cacheDuration;
  }

  @Override
  public int getSchedulerThreadPoolSize() {
    return this.threadPoolSize;
  }

  @Override
  public Duration getNameCacheDuration() {
    return this.nameCacheDuration;
  }

  @Override
  public String getMoneyNameSingular() {
    return this.moneyNameSingular;
  }

  @Override
  public String getMoneyNamePlural() {
    return this.moneyNamePlural;
  }

  @Override
  public double getMoneyCombinationRadius() {
    return this.moneyCombinationRadius;
  }

  @Override
  public double getItemInterestRate() {
    return this.adminShopInterestRate;
  }
}
