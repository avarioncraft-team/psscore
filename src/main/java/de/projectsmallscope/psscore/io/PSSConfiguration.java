package de.projectsmallscope.psscore.io;

import java.time.Duration;

/*******************************************************
 * Copyright (C) Gestankbratwurst suotokka@gmail.com
 *
 * This file is part of PSSCore and was created at the 22.10.2020
 *
 * PSSCore can not be copied and/or distributed without the express
 * permission of the owner.
 *
 */

public interface PSSConfiguration {

  String getMongoURI();

  String getDBName();

  String getResourcePackServerIP();

  int getResourcePackServerPort();

  Duration getPlayerCacheDuration();

  int getSchedulerThreadPoolSize();

  Duration getNameCacheDuration();

  String getMoneyNameSingular();

  String getMoneyNamePlural();

  double getMoneyCombinationRadius();

  double getItemInterestRate();

}